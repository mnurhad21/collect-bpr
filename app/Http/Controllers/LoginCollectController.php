<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;  
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
//use Input;
use Validator;
use Redirect;
use Session;
use Auth;

class LoginCollectController extends BaseController {
    public function showLoginCollect() {
        return view("login.login-page");
    }

    public function processLogin() {
        if(null === Input::get("mobile") || trim(Input::get("mobile")) === "") {
            $accessMode = "WEB";
        }
        else {
            $accessMode = "MOBILE";
        }

        if(null === Input::get("loginEmail") || trim(Input::get("loginEmail")) === "") {
            if($accessMode == "WEB") {
                return Redirect::to('login')->with('ctlError','Periksa kembali data login Anda');
            }
            else {
                return composeReply("ERROR", "Periksa kembali data login Anda");
            }
        }

        $loginEmail = Input::post("loginEmail");
        $loginPassword = Input::post("loginPassword");
        //Log::info('loginEmail : '.$loginEmail.' - loginPassword : '.$loginPassword);
        // select query dengan parameter
        $errMsg = "";
        $users = DB::select("SELECT * FROM coll_user WHERE (U_ID = ? OR U_EMAIL = ? OR U_TELPON = ?) AND U_STATUS = 'USER_ACTIVE'", array($loginEmail, $loginEmail, $loginEmail));
        if(count($users) > 0) { 
            $userId = $users[0]->{"U_ID"};
            if($users[0]->{"U_PASSWORD_HASH"} !== md5($userId.$loginPassword))  $errMsg = "Periksa kembali data login Anda";

            if($users[0]->{"PRSH_ID"} != "-") {
                $prshData = DB::table("coll_perusahaan")->where("PRSH_ID", $users[0]->{"PRSH_ID"})->first();
                //if($prshData)                                       $errMsg = "Perusahaan user tidak dikenal";
                if($prshData->{"PRSH_STATUS_AKTIF"} != "Y") $errMsg = "Perusahaan user tidak aktif"; 
            }   

            if($errMsg == "") {
                $loginToken = substr(md5($users[0]->{"U_NAMA"}.date("Y-m-d H:i:s")), 0,30);
                DB::table("coll_user")->where("U_ID",$users[0]->{"U_ID"})->update(array(
                    'U_LOGIN_TOKEN' => $loginToken,
                    'U_LOGIN_WAKTU' => date("Y-m-d H:i:s")
                ));

                if($accessMode == "WEB") {
                    Session::put('SESSION_USER_NAME', $users[0]->{"U_NAMA"});
                    Session::put('SESSION_USER_ID', $users[0]->{"U_ID"});
                    Session::put('SESSION_USER_ROLE', $users[0]->{"U_GROUP_ROLE"});
                    Session::put('SESSION_LOGIN_TOKEN', $loginToken);
                    if(isset($prshData)) {
                        Session::put('SESSION_COMPANY_NAME', $prshData->{"PRSH_NAMA"});
                        Session::put('SESSION_COMPANY_ID', $users[0]->{"PRSH_ID"});
                        Session::put('SESSION_COMPANY_ADDRESS', $prshData->{"PRSH_ALAMAT"});
                        Session::put('SESSION_COMPANY_CITY', $prshData->{"PRSH_KOTA"});
                    }
                    
                    return Redirect::to("dashboard")    
                        ->with("ctlLogin",$users[0]->{"U_ID"})
                        ->with("ctlUserName",$users[0]->{"U_NAMA"});
                }
                else {
                    return composeReply2("SUCCESS", "Login sukses", array(
                        'LOGIN_TOKEN' => $loginToken,
                        'LOGIN_USER_NAME' => $users[0]->{"U_NAMA"},
                        'LOGIN_USER_ID' => $users[0]->{"U_ID"},
                        'LOGIN_USER_ROLE' => $users[0]->{"U_GROUP_ROLE"},
                        'LOGIN_COMPANY_NAME' => $prshData->{"PRSH_NAMA"},
                        'LOGIN_COMPANY_ID' => $users[0]->{"PRSH_ID"},
                        'LOGIN_EMAIL' => $users[0]->{"U_EMAIL"},
                        'LOGIN_PHONE' => $users[0]->{"U_TELPON"}
                    ));
                }
            }
            else {
                if($accessMode == "WEB") {
                    //automatically set session named ctlError
                    return Redirect::to('login')->with('ctlError', $errMsg);
                }
                else {
                    return composeReply("ERROR", $errMsg);
                }
            }
        }
        else {
            if($accessMode == "WEB") {
                //automatically set session named ctlError
                return Redirect::to('login')->with('ctlError','Akun salah atau akun tidak aktif');
            }
            else {
                return composeReply("ERROR", "Akun salah atau akun tidak aktif");
            }
        }
    }

    public function processLogout() {
        Session::flush();
        return Redirect::to('/');
    }

    public function checkToken() {
        if(null === Input::get("userId") || trim(Input::get("userId")) === "")                  return composeReply("ERROR", "Invalid user ID");
        if(null === Input::get("loginToken") || trim(Input::get("loginToken")) === "")  return composeReply("ERROR", "Invalid login token");
        if(!isLoginValid(Input::get('userId'), Input::get('loginToken')))                               return composeReply("ERROR", "Invalid login token");
        
        $userData = DB::table("coll_user")
        ->where("U_ID", Input::get("userId"))
        ->where("U_LOGIN_TOKEN", Input::get("loginToken"))
        ->first();

      if(count($userData) <= 0) return composeReply("ERROR", "Invalid #1");

      //$arrDiff = array();
      //$arrDiff = dayDifference2(date("Y-m-d H:i:s"),$userData->{"U_LOGIN_WAKTU"},TRUE);
      
        //return composeReply("SUCCESS", "Days since last login : ".$arrDiff["DAY"]);
        if(isLoginValid(Input::get("userId"), Input::get("loginToken"))) {
            return composeReply("SUCCESS", "Login VALID");
        }
        else {
            return composeReply("ERROR", "Login EXPIRED");
        }
    }
}
?>