<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;  // <<< See here - no real class, only an alias
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
//use Input;
use Validator;
use Redirect;
use Session;
use Auth;

class CompanyController extends BaseController {
	public function redirectMissing() {
		Session::flush();
		return Redirect::to('login');
	}

	public function listCompanies() {
		if (Session::has('SESSION_USER_ID')) {
			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			$prsh = DB::table("coll_perusahaan")->get();

			$jml = (count($prsh)) + 1;
			if($jml < 10)	$jml = "00".$jml;
			if($jml > 10 && $jml < 100)	$jml = "0".$jml;

			foreach ($prsh as $aData) {
				$spv = DB::select("SELECT IFNULL(COUNT(U_ID),0) AS JUMLAH FROM coll_user WHERE PRSH_ID = ? AND U_GROUP_ROLE = 'GR_SUPERVISOR'", array($aData->{"PRSH_ID"}));
				$aData->{"JUMLAH_SPV"} = $spv[0]->{"JUMLAH"};

				$coll = DB::select("SELECT IFNULL(COUNT(U_ID),0) AS JUMLAH FROM coll_user WHERE PRSH_ID = ? AND U_GROUP_ROLE = 'GR_COLLECTOR'", array($aData->{"PRSH_ID"}));
				$aData->{"JUMLAH_COLL"} = $coll[0]->{"JUMLAH"};

				$data = DB::select("SELECT IFNULL(COUNT(BUD_ID),0) AS JUMLAH FROM coll_batch_upload_data WHERE PRSH_ID = ?", array($aData->{"PRSH_ID"}));
				$aData->{"JUMLAH_DATA"} = $data[0]->{"JUMLAH"};
			}

			return view("dashboard.comp-mgmt.comp-formlist")	
				->with("ctlUserData", $userData)
				->with("ctlPrsh", $prsh)
				->with("ctlUrutan", $jml)
				->with("ctlNavMenu", "mCompMgmt");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}
	}

	public function addCompany() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("prshId") || trim(Input::get("prshId")) === "")	return composeReply("ERROR", "ID perusahaan harus diisi");
			$prshId = strtoupper(preg_replace('/\s+/', '', getSetting("COMPANY_PREFIX").Input::get("prshId")));
			$prshData = DB::table('coll_perusahaan')->where('PRSH_ID', $prshId)->first();
			if(count($prshData) > 0)	return composeReply("ERROR", "ID perusahaan sudah digunakan");

			if(null === Input::get("prshNama") || trim(Input::get("prshNama")) === "")				return composeReply("ERROR", "Nama perusahaan harus diisi");				
			if(null === Input::get("prshAlamat") || trim(Input::get("prshAlamat")) === "")		return composeReply("ERROR", "Alamat perusahaan harus diisi");				
			if(null === Input::get("prshKota") || trim(Input::get("prshKota")) === "")				return composeReply("ERROR", "Kota perusahaan harus diisi");
			if(null === Input::get("prshPICNama") || trim(Input::get("prshPICNama")) === "")	return composeReply("ERROR", "Nama penanggung jawab harus diisi");
			if(null === Input::get("prshPICTelp") || trim(Input::get("prshPICTelp")) === "")	return composeReply("ERROR", "Nomor telepon penanggung jawab harus diisi");

			$opr = DB::table("coll_perusahaan")->insert(array(
				'PRSH_ID' => $prshId,
				'PRSH_NAMA' => Input::get("prshNama"),
				'PRSH_ALAMAT' => Input::get("prshAlamat"),
				'PRSH_KOTA' => Input::get("prshKota"),
				'PRSH_PIC_NAMA' => Input::get("prshPICNama"),
				'PRSH_PIC_TELP' => Input::get("prshPICTelp"),
				'PRSH_STATUS_AKTIF' => 'Y',
				'PRSH_REGISTRAR_U_ID' => $userId
			));

			if(isset($_FILES['imgFile'])){
        $fileName = $_FILES['imgFile']['name'];
        $fileSize = $_FILES['imgFile']['size'];
        $fileTmp = $_FILES['imgFile']['tmp_name'];
        $fileType = $_FILES['imgFile']['type'];
        $a = explode(".", $_FILES["imgFile"]["name"]);
        $fileExt = strtolower(end($a));

        $arrFileExt = array("jpg","jpeg","png","JPG","JPEG","PNG");
        if(isset($fileName) && trim($fileName) != "") {
          if(in_array($fileExt,$arrFileExt)=== false)   return composeReply("ERROR","Harap pilih file JPG atau PNG");
          if($fileSize > 2048000)                       return composeReply("ERROR","Harap pilih file JPG atau PNG dengan ukuran max. 2 MB");
          
          $uploadFile = "uploads/logo-".substr(md5(date("YmdHis")),0,10).".".$fileExt;
          if(move_uploaded_file($fileTmp,$uploadFile) == TRUE) {    
            DB::table("coll_perusahaan")->where("PRSH_ID", $prshId)->update(array(
            	'PRSH_IMG_PATH' => $uploadFile
            ));
            return composeReply("SUCCESS", "Data perusahaan telah disimpan beserta logo perusahaan");
          }         
        }
        else {
          return composeReply("ERROR","Data perusahaan telah disimpan namun proses upload gagal (file upload tidak terdeteksi server)");
        }
      }
			
			//if($opr == true) {
				return composeReply("SUCCESS", "Data perusahaan telah disimpan tanpa logo perusahaan");
			//}
			//else {
			//	return composeReply("ERROR", "Gagal menyimpan data perusahaan");
			//}
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}	
	}

	public function updateCompany() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("prshId") || trim(Input::get("prshId")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			
			$prshData = DB::table('coll_perusahaan')->where('PRSH_ID',Input::get("prshId"))->first();
			if(count($prshData) <= 0)	return composeReply("ERROR", "Perusahaan tidak dikenal");

			if(null === Input::get("prshNama") || trim(Input::get("prshNama")) === "")				return composeReply("ERROR", "Nama perusahaan harus diisi");				
			if(null === Input::get("prshAlamat") || trim(Input::get("prshAlamat")) === "")		return composeReply("ERROR", "Alamat perusahaan harus diisi");				
			if(null === Input::get("prshKota") || trim(Input::get("prshKota")) === "")				return composeReply("ERROR", "Kota perusahaan harus diisi");
			if(null === Input::get("prshPICNama") || trim(Input::get("prshPICNama")) === "")	return composeReply("ERROR", "Nama penanggung jawab harus diisi");
			if(null === Input::get("prshPICTelp") || trim(Input::get("prshPICTelp")) === "")	return composeReply("ERROR", "Nomor telepon penanggung jawab harus diisi");
			//if(null === Input::get("prshStatus") || trim(Input::get("prshStatus")) === "")		return composeReply("ERROR", "Status perusahaan harus diisi");

			DB::table("coll_perusahaan")->where("PRSH_ID", Input::get("prshId"))->update(array(
				'PRSH_NAMA' => Input::get("prshNama"),
				'PRSH_ALAMAT' => Input::get("prshAlamat"),
				'PRSH_KOTA' => Input::get("prshKota"),
				'PRSH_PIC_NAMA' => Input::get("prshPICNama"),
				'PRSH_PIC_TELP' => Input::get("prshPICTelp")/*,
				'PRSH_STATUS_AKTIF' => Input::get("prshStatus")*/
			));
			
			return composeReply("SUCCESS", "Perubahan data perusahaan telah disimpan");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}	
	}

	public function deleteCompany() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			$prshData = DB::table('coll_perusahaan')->where('PRSH_ID',Input::get("id"))->first();
			if(count($prshData) <= 0)	return composeReply("ERROR", "Perusahaan tidak dikenal");

			DB::table("coll_batch_upload")->where("PRSH_ID", Input::get("id"))->delete();
			DB::table("coll_batch_upload_data")->where("PRSH_ID", Input::get("id"))->delete();
			DB::table("coll_user")->where("PRSH_ID", Input::get("id"))->delete();
			DB::table("coll_perusahaan")->where("PRSH_ID", Input::get("id"))->delete();
			
			return composeReply("SUCCESS", "Data perusahaan dan semua turunannya telah dihapus");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}		
	}

	public function getCompanyData() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");

			$prshData = DB::table('coll_perusahaan')->where('PRSH_ID',Input::get("id"))->first();
			if(count($prshData) <= 0)	return composeReply("ERROR", "Perusahaan tidak terdaftar");

			return composeReply("SUCCESS", "Company data", $prshData);
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}	
	}

	public function updateCompanyStatus() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			$prshData = DB::table('coll_perusahaan')->where('PRSH_ID',Input::get("id"))->first();
			if(count($prshData) <= 0)	return composeReply("ERROR", "Perusahaan tidak terdaftar");

			if(null === Input::get("prshStatus") || trim(Input::get("prshStatus")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			if(strtoupper(trim(Input::get("prshStatus"))) == "AKTIF") {
				$status = "Y";
			}
			elseif(strtoupper(trim(Input::get("prshStatus"))) == "NON_AKTIF") {
				$status = "T";
			}
			else {
				return composeReply("ERROR", "Parameter tidak sesuai");
			}

			DB::table("coll_perusahaan")->where("PRSH_ID", Input::get("id"))->update(array(
				'PRSH_STATUS_AKTIF' => $status
			));
			
			return composeReply("SUCCESS", "Perubahan status perusahaan telah disimpan");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}		
	}
    //dummy function
}
?>