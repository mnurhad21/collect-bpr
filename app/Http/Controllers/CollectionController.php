<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;  // <<< See here - no real class, only an alias
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
//use Input;
use Validator;
use Redirect;
use Session;
use Auth;

class CollectionController extends BaseController {
  public function redirectMissing() {
    Session::flush();
    return Redirect::to('login');
  }

  public function formlistJadwal() {    
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(null === Input::get("periode") || trim(Input::get("periode")) === "") {
        $periode = date("Y")."-".date("m");
      }
      else {
        $periode = Input::get("periode");
      }

      $arrPeriode = explode("-", $periode);
      $month = $arrPeriode[1];
      $year = $arrPeriode[0];

      $uploads = DB::select("SELECT * FROM coll_batch_upload WHERE MONTH(BU_TGL) = ? AND YEAR(BU_TGL) = ? AND PRSH_ID = ?", array($month, $year, $userData->{"PRSH_ID"}));
      foreach ($uploads as $aData) {
        $data = DB::select("SELECT IFNULL(COUNT(BUD_ID),0) AS JUMLAH FROM coll_batch_upload_data WHERE BU_ID = ?", array($aData->{"BU_ID"}));
        $aData->{"JUMLAH_DATA"} = $data[0]->{"JUMLAH"};

        $tagihan = DB::select("SELECT IFNULL(SUM(BUD_PINJ_JUMLAH),0) AS JUMLAH FROM coll_batch_upload_data WHERE BU_ID = ?", array($aData->{"BU_ID"}));
        $aData->{"JUMLAH_TAGIHAN"} = $tagihan[0]->{"JUMLAH"};

        $bayar = DB::select("SELECT IFNULL(SUM(BUD_PINJ_JUMLAH_BAYAR),0) AS JUMLAH FROM coll_batch_upload_data WHERE BU_ID = ?", array($aData->{"BU_ID"}));
        $aData->{"JUMLAH_BAYAR"} = $bayar[0]->{"JUMLAH"};       
      }
      
      return View::make("dashboard.collection.jadwal-formlist") 
        ->with("ctlUserData", $userData)
        ->with("ctlFilterMonth",$month)
        ->with("ctlFilterYear", $year)
        ->with("ctlUploads", $uploads)
        ->with("ctlNavMenu", "mCollJadwal");
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    }
  }

  public function submitJadwal() {
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(isset($_FILES['jdwFile'])){
        $fileName = $_FILES['jdwFile']['name'];
        $fileSize = $_FILES['jdwFile']['size'];
        $fileTmp = $_FILES['jdwFile']['tmp_name'];
        $fileType = $_FILES['jdwFile']['type'];
        $a = explode(".", $_FILES["jdwFile"]["name"]);
        $fileExt = strtolower(end($a));

        $arrFileExt = array("xls","xlsx","XLS","XLSX");
        if(isset($fileName) && trim($fileName) != "") {
          if(in_array($fileExt,$arrFileExt)=== false)   return composeReply("ERROR","Harap pilih file Excel");
          if($fileSize > 2048000)                       return composeReply("ERROR","Harap pilih file Excel dengan ukuran max. 2 MB");
          
          $uploadFile = "uploads/jadwal-".createSlug($userId)."-".date("YmdHis").".".$fileExt;
          if(move_uploaded_file($fileTmp,$uploadFile) == TRUE) {    
            DB::beginTransaction();

            $buId = DB::table("coll_batch_upload")->insertGetId(array(
              'BU_TGL' => date("Y-m-d"),
              'BU_FILE_PATH' => $uploadFile,
              'PRSH_ID' => $userData->{"PRSH_ID"},
              'U_ID' => $userData->{"U_ID"}
            ));
            if(!isset($buId) || $buId <= 0) {
              DB::rollback();
              return composeReply("ERROR", "Proses penyimpanan data jadwal mengalami kegagalan");
            }

            $objPHPExcel = PHPExcel_IOFactory::load($uploadFile);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

            $titles = $objWorksheet->rangeToArray('A1:' . $highestColumn . "1");
            $body = $objWorksheet->rangeToArray('A2:' . $highestColumn . $highestRow);
            $table = array();
            //return composeReply("ERROR", "HIT HERE : ".$highestRow);
            for ($row=0; $row <= $highestRow - 2; $row++) {
              $a = array();
              for ($column=0; $column <= $highestColumnIndex - 1; $column++) {
                if($column == 0 && $titles[0][$column] != "ID_COLLECTOR")   return composeReply("ERROR","Kolom ke-1 HARUS bernama ID_COLLECTOR");
                if($column == 1 && $titles[0][$column] != "NAMA_COLLECTOR") return composeReply("ERROR","Kolom ke-2 HARUS bernama NAMA_COLLECTOR");
                if($column == 2 && $titles[0][$column] != "ID_PINJAMAN")    return composeReply("ERROR","Kolom ke-3 HARUS bernama ID_PINJAMAN");
                if($column == 3 && $titles[0][$column] != "ID_CUSTOMER")    return composeReply("ERROR","Kolom ke-4 HARUS bernama ID_CUSTOMER");
                if($column == 4 && $titles[0][$column] != "PERIODE")        return composeReply("ERROR","Kolom ke-5 HARUS bernama PERIODE");
                if($column == 5 && $titles[0][$column] != "NAMA")           return composeReply("ERROR","Kolom ke-6 HARUS bernama NAMA");
                if($column == 6 && $titles[0][$column] != "ALAMAT")         return composeReply("ERROR","Kolom ke-7 HARUS bernama ALAMAT");
                if($column == 7 && $titles[0][$column] != "NO_HP")          return composeReply("ERROR","Kolom ke-8 HARUS bernama NO_HP");
                if($column == 8 && $titles[0][$column] != "MASA_KREDIT")    return composeReply("ERROR","Kolom ke-9 HARUS bernama MASA_KREDIT");
                if($column == 9 && $titles[0][$column] != "TGL_KREDIT")     return composeReply("ERROR","Kolom ke-10 HARUS bernama TGL_KREDIT");
                if($column == 10 && $titles[0][$column] != "TGL_ANG")       return composeReply("ERROR","Kolom ke-11 HARUS bernama TGL_ANG");
                if($column == 11 && $titles[0][$column] != "TGL_JDWL")      return composeReply("ERROR","Kolom ke-12 HARUS bernama TGL_JDWL");
                if($column == 12 && $titles[0][$column] != "BY_POKOK")      return composeReply("ERROR","Kolom ke-13 HARUS bernama BY_POKOK");
                if($column == 13 && $titles[0][$column] != "BUNGA")         return composeReply("ERROR","Kolom ke-14 HARUS bernama BUNGA");
                if($column == 14 && $titles[0][$column] != "DENDA")         return composeReply("ERROR","Kolom ke-15 HARUS bernama DENDA");
                if($column == 15 && $titles[0][$column] != "BY_JUMLAH")     return composeReply("ERROR","Kolom ke-16 HARUS bernama BY_JUMLAH");
                
                $a[$titles[0][$column]] = $body[$row][$column];
              }
              $table[$row] = $a;
              if(isset($table[$row]) && trim($table[$row]["ID_CUSTOMER"]) !== "") {
                //return composeReply("ERROR", "HIT : ".$table[$row]["ID_CUSTOMER"]);
                $separator = "";
                if(strpos($table[$row]["TGL_KREDIT"], "-") !== false) $separator = "-";
                if(strpos($table[$row]["TGL_KREDIT"], "/") !== false) $separator = "/";

                if($separator == "")  {
                  //return composeReply("ERROR", " ROW : ".$row);
                  break;
                }

                $arrTgl = explode($separator, $table[$row]["TGL_KREDIT"]); //menjadi 02-24-17
                $tglKredit = "20".trim($arrTgl[2])."-".trim($arrTgl[0])."-".trim($arrTgl[1]);
                //return composeReply("ERROR", "TGL_KREDIT : ".$tglKredit);

                if(strpos($table[$row]["TGL_ANG"], "-") !== false) $separator = "-";
                if(strpos($table[$row]["TGL_ANG"], "/") !== false) $separator = "/";
                $arrTgl = explode("-", $table[$row]["TGL_ANG"]); //menjadi 02-24-17
                $tglAngsur = "20".trim($arrTgl[2])."-".trim($arrTgl[0])."-".trim($arrTgl[1]);;

                if(strpos($table[$row]["TGL_JDWL"], "-") !== false) $separator = "-";
                if(strpos($table[$row]["TGL_JDWL"], "/") !== false) $separator = "/";
                $arrTgl = explode("-", $table[$row]["TGL_JDWL"]); //menjadi 02-24-17
                $tglJadwal = "20".trim($arrTgl[2])."-".trim($arrTgl[0])."-".trim($arrTgl[1]);
                //return composeReply("ERROR", "TGL_JDWL : ".$tglJadwal);

                $bayarPokok = str_replace(",", "", $table[$row]["BY_POKOK"]);
                $bayarPokok = str_replace(".", "", $bayarPokok);

                $bayarJumlah = str_replace(",", "", $table[$row]["BY_JUMLAH"]);
                $bayarJumlah = str_replace(".", "", $bayarJumlah);

                $bayarBunga = str_replace(",", "", $table[$row]["BUNGA"]);
                $bayarBunga = str_replace(".", "", $bayarBunga);

                $bayarDenda = str_replace(",", "", $table[$row]["DENDA"]);
                $bayarDenda = str_replace(".", "", $bayarDenda);

                //cek dl apakah sdh ada user kolektor
                $kolektor = DB::table("coll_user")->where("U_ID",trim($table[$row]["ID_COLLECTOR"]))->first();
                if(count($kolektor) <= 0) {
                  $randomTelp = "99".randomDigits(10);
                  $randomEmail = strtolower(trim($table[$row]["ID_COLLECTOR"]))."@".strtolower($userData->{"PRSH_ID"});
                  
                  DB::table("coll_user")->insert(array(
                    'U_ID' => trim($table[$row]["ID_COLLECTOR"]),
                    'U_PASSWORD' => getSetting("DEFAULT_PASSWORD"),
                    'U_PASSWORD_HASH' => md5(trim($table[$row]["ID_COLLECTOR"]).getSetting("DEFAULT_PASSWORD")),
                    'U_NAMA' => $table[$row]["NAMA_COLLECTOR"],
                    'U_GROUP_ROLE' => 'GR_COLLECTOR',
                    'U_TELPON' => $randomTelp,
                    'U_EMAIL' => $randomEmail,
                    'U_STATUS' => 'USER_ACTIVE',
                    'U_REGISTRASI_TGL' => date("Y-m-d H:i:s"),
                    'PRSH_ID' => $userData->{"PRSH_ID"}
                  ));
                }
                else { //cek apakah ada perubahan nama
                  if(trim(strtoupper($kolektor->{"U_NAMA"})) != trim(strtoupper($table[$row]["NAMA_COLLECTOR"]))) {
                    DB::table("coll_user")->where("U_ID",trim($table[$row]["ID_COLLECTOR"]))->update(array(
                      'U_NAMA' => $table[$row]["NAMA_COLLECTOR"]
                    ));
                  }
                }

                //cek dl apakah sdh ada data nasabah
                $nasabah = DB::table("coll_customers")->where("CUST_ID", trim($table[$row]["ID_CUSTOMER"]))->first();
                if(count($nasabah) <= 0) {
                  DB::table("coll_customers")->insert(array(
                    'CUST_ID' => trim($table[$row]["ID_CUSTOMER"]),
                    'CUST_NAMA' => trim($table[$row]["NAMA"]),
                    'CUST_ALAMAT' => trim($table[$row]["ALAMAT"]),
                    'CUST_PONSEL' => trim($table[$row]["NO_HP"])
                  ));
                }
                else { //cek apakah ada perubahan data
                  if(trim(strtoupper($nasabah->{"CUST_NAMA"})) != trim(strtoupper($table[$row]["NAMA"]))) {
                    DB::table("coll_customers")->where("CUST_ID",trim($table[$row]["ID_CUSTOMER"]))->update(array(
                      'CUST_NAMA' => $table[$row]["NAMA"]
                    ));
                  }
                  if(trim(strtoupper($nasabah->{"CUST_ALAMAT"})) != trim(strtoupper($table[$row]["ALAMAT"]))) {
                    DB::table("coll_customers")->where("CUST_ID",trim($table[$row]["ID_CUSTOMER"]))->update(array(
                      'CUST_ALAMAT' => $table[$row]["ALAMAT"]
                    ));
                  }
                  if(trim(strtoupper($nasabah->{"CUST_PONSEL"})) != trim(strtoupper($table[$row]["NO_HP"]))) {
                    DB::table("coll_customers")->where("CUST_ID",trim($table[$row]["ID_CUSTOMER"]))->update(array(
                      'CUST_PONSEL' => $table[$row]["NO_HP"]
                    ));
                  }
                }

                //cek dl apakah sdh ada data pinjaman
                $pinjaman = DB::table("coll_pinjaman")->where("PINJ_ID", trim($table[$row]["ID_PINJAMAN"]))->first();
                if(count($pinjaman) <= 0) {
                  DB::table("coll_pinjaman")->insert(array(
                    'PINJ_ID' => trim($table[$row]["ID_PINJAMAN"]),
                    'CUST_ID' => trim($table[$row]["ID_CUSTOMER"]),
                    'PINJ_MASA_KREDIT' => trim($table[$row]["MASA_KREDIT"]),
                    'PINJ_TGL_KREDIT' => $tglKredit
                  ));
                }

                $budId = DB::table("coll_batch_upload_data")->insertGetId(array(
                  'BU_ID' => $buId,
                  'BUD_COLL_U_ID' => trim($table[$row]["ID_COLLECTOR"]),
                  'BUD_PINJ_ID' => trim($table[$row]["ID_PINJAMAN"]),
                  'BUD_PINJ_PERIODE' => $table[$row]["PERIODE"],
                  'BUD_CUST_ID' => trim($table[$row]["ID_CUSTOMER"]),
                  'BUD_CUST_NAMA' => trim($table[$row]["NAMA"]),
                  'BUD_CUST_ALAMAT' => trim($table[$row]["ALAMAT"]),
                  'BUD_CUST_PONSEL' => trim($table[$row]["NO_HP"]),
                  'BUD_PINJ_MASA_KREDIT' => trim($table[$row]["MASA_KREDIT"]),
                  'BUD_PINJ_TGL_KREDIT' => $tglKredit,
                  'BUD_PINJ_TGL_ANGS' => $tglAngsur,
                  'BUD_PINJ_TGL_JADWAL' => $tglJadwal,
                  'BUD_PINJ_POKOK' => $bayarPokok,
                  'BUD_PINJ_BUNGA' => $bayarBunga,
                  'BUD_PINJ_DENDA' => $bayarDenda,
                  'BUD_PINJ_JUMLAH' => $bayarJumlah,
                  'BUD_PINJ_TGL_BAYAR' => "0000-00-00 00:00:00",
                  'BUD_PINJ_JUMLAH_BAYAR' => '0',
                  'BUD_STATUS' => 'ST_JADWAL',
                  'BUD_LOKASI_LAT' => '0',
                  'BUD_LOKASI_LNG' => '0',
                  'PRSH_ID' => $userData->{"PRSH_ID"}
                ));
                if(!isset($budId) || $budId <= 0) {
                  DB::rollback();
                  return composeReply("ERROR", "Proses penyimpanan data jadwal mengalami kegagalan");
                }

                //generate jadwal 
                //TO DO : more complex consideration
                //misal : data cust id dan pinj id sama, tp periode beda
                for($i=0; $i<intval($table[$row]["PERIODE"]); $i++) {
                  $aTgl = addDaysWithDate($tglJadwal,$i,'Y-m-d');
                  $cek = DB::table("coll_jadwal")
                    ->where("J_TGL", $aTgl)
                    ->where("CUST_ID", trim($table[$row]["ID_CUSTOMER"]))
                    ->where("PINJ_ID", trim($table[$row]["ID_PINJAMAN"]))
                    ->where("J_PINJ_JUMLAH", $bayarJumlah)
                    ->first();

                  if(count($cek) <= 0) {
                    $jId = DB::table("coll_jadwal")->insertGetId(array(
                      'J_TGL' => $aTgl,
                      'BU_ID' => $buId,
                      'BUD_ID' => $budId,
                      'CUST_ID' => trim($table[$row]["ID_CUSTOMER"]),
                      'PINJ_ID' => trim($table[$row]["ID_PINJAMAN"]),
                      'J_PINJ_JUMLAH' => $bayarJumlah,
                      'J_PINJ_JUMLAH_BAYAR' => '0',
                      'J_STATUS' => 'ST_JADWAL',
                      'PRSH_ID' => $userData->{"PRSH_ID"},
                      'J_COLL_U_ID' =>trim($table[$row]["ID_COLLECTOR"])
                    ));
                  }
                  else {
                    DB::table("coll_jadwal")
                      ->where("J_TGL", $aTgl)
                      ->where("CUST_ID", trim($table[$row]["ID_CUSTOMER"]))
                      ->where("PINJ_ID", trim($table[$row]["ID_PINJAMAN"]))
                      ->where("J_PINJ_JUMLAH", $bayarJumlah)
                      ->update(array(
                          'BUD_ID' => $budId,
                          'BU_ID' => $budId
                        ));
                  }
                }
              }
            }
            DB::commit();
            return composeReply("SUCCESS", "Data telah disimpan");
            //return composeReply("SUCCESS", "Hasil : ".$table[0]["ID_COLLECTOR"]." - ".$table[0]["NAMA_COLLECTOR"]." - ".$tglKredit." - ".$tglAngsur." - ".$tglJadwal." - ".$bayarPokok." - ".$bayarJumlah);
          }         
        }
        else {
          return composeReply("ERROR","Proses upload gagal (file upload tidak terdeteksi server)");
        }
      }
      else {
        return composeReply("ERROR","Harap sertakan file untuk diunggah");
      }
    }
    else {
      Session::flush();
      return composeReply("ERROR","Silahkan login terlebih dahulu");
    }
  }

  public function deleteJadwal() {
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(count($userData) <= 0) return composeReply("ERROR", "User tidak dikenal");

      if(null === Input::get("id") || trim(Input::get("id")) === "")  return composeReply("ERROR", "Parameter tidak lengkap");

      $bu = DB::table("coll_batch_upload")->where("BU_ID", Input::get("id"))->first();
      if(count($bu) <= 0) return composeReply("ERROR", "Data batch upload tidak dikenal");
      File::delete(asset_url()."/".$bu->{"BU_FILE_PATH"});

      $bud = DB::table("coll_batch_upload_data")
        ->where("BU_ID", Input::get("id"))
        ->where("BUD_IMG_PATH", "!=", "-")
        ->get();
      foreach ($bud as $aData) {
        File::delete(asset_url()."/".$bud->{"BUD_IMG_PATH"});
      }

      DB::table("coll_jadwal")->where("BU_ID", Input::get("id"))->delete();
      DB::table("coll_batch_upload_data")->where("BU_ID", Input::get("id"))->delete();
      DB::table("coll_batch_upload")->where("BU_ID", Input::get("id"))->delete();

      return composeReply("SUCCESS", "Data telah dihapus");
    }
    else {
      Session::flush();
      return composeReply("ERROR","Silahkan login terlebih dahulu");  
    }
  }

  public function listDetailJadwal($buId) {
    if(null === $buId || !isset($buId)) {
      return Redirect::to("collection/jadwal-penagihan");
    }

    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      $bud = DB::select("SELECT A.*, B.U_NAMA, C.R_INFO FROM coll_batch_upload_data AS A INNER JOIN coll_user AS B ON A.BUD_COLL_U_ID = B.U_ID INNER JOIN coll_referensi AS C ON A.BUD_STATUS = C.R_ID WHERE A.BU_ID = ?", array($buId));
      
      return View::make("dashboard.collection.jadwal-detail-list")  
        ->with("ctlUserData", $userData)
        ->with("ctlBUD", $bud)
        ->with("ctlBuId", $buId)
        ->with("ctlNavMenu", "mCollJadwal");
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    } 
  }

  public function displayMonitoring() {
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();
      
      //$jadwal = DB::select("SELECT * FROM coll_batch_upload WHERE PRSH_ID = ? AND MONTH(BU_TGL) = ? AND YEAR(BU_TGL) = ? ORDER BY BU_TGL DESC", array($userData->{"PRSH_ID"}, date("m"), date("Y")));
      $jadwal = DB::select("SELECT * FROM coll_batch_upload WHERE PRSH_ID = ? ORDER BY BU_ID DESC", array($userData->{"PRSH_ID"}));

      return View::make("dashboard.collection.monitor-display") 
        ->with("ctlUserData", $userData)
        ->with("ctlJadwal", $jadwal)
        ->with("ctlNavMenu", "mCollMonitoring");
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    } 
  }

  public function getPosition() {
    $reply = "";
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(null === Input::get("periode") || trim(Input::get("periode")) === "") {
        $periode = date("Y-m-d");
        $periode_dmY = date("d-m-Y");
      }
      else {
        $periode = Input::get("periode");
        $arrTgl = explode("-", $periode);
        $periode_dmY = $arrTgl[2]."-".$arrTgl[1]."-".$arrTgl[0];
      }
      
      if(null === Input::get("jadwal") || trim(Input::get("jadwal")) === "") {
        $latestBatchUpload = DB::select("SELECT BU_ID FROM coll_batch_upload WHERE PRSH_ID = ? AND BU_TGL <= ? ORDER BY BU_ID DESC LIMIT 0,1", array($userData->{"PRSH_ID"}, $periode));
        $buId = $latestBatchUpload[0]->{"BU_ID"};
      }
      else {
        $buId = Input::get("jadwal");
      }
      
      //baris brkt utk menampilkan check in markers yg masih valid pd BU_ID yg latest
      //$collRecords = DB::select("SELECT A.*, B.BU_TGL, C.U_NAMA FROM coll_batch_upload_data AS A INNER JOIN coll_batch_upload AS B ON A.BU_ID = B.BU_ID INNER JOIN coll_user AS C ON A.BUD_COLL_U_ID = C.U_ID WHERE B.BU_ID = ? AND DATE_FORMAT(A.BUD_PINJ_TGL_JADWAL + A.BUD_PINJ_PERIODE, '%Y-%m-%d') <= ? ORDER BY A.BUD_COLL_U_ID", array($latestBatchUpload[0]->{"BU_ID"}, date("Y-m-d")));
      
      //baris brkt utk menampilkan SEMUA check in markers pd BU_ID yg latest
      $collRecords = DB::select("SELECT A.*, B.BU_TGL, C.U_NAMA FROM coll_batch_upload_data AS A INNER JOIN coll_batch_upload AS B ON A.BU_ID = B.BU_ID INNER JOIN coll_user AS C ON A.BUD_COLL_U_ID = C.U_ID WHERE B.BU_ID = ? ORDER BY A.BU_ID,A.BUD_COLL_U_ID, A.BUD_STATUS_WAKTU", array($buId));

      $reply = "";
      $lastBatchUpload = "";
      $lastCollector = "";
      $lastWaktu = "";
      if(count($collRecords) > 0) {        
        foreach ($collRecords as $aData) {
          //check in start
          //if($aData->{"BUD_STATUS"} != "ST_JADWAL") { //hanya tampilkan yg sdh ada action 
          if($aData->{"BUD_LOKASI_LAT"} != "0" && $aData->{"BUD_LOKASI_LNG"} != "0") {
            if($lastBatchUpload == $aData->{"BU_ID"} && $lastCollector == $aData->{"BUD_COLL_U_ID"}) {
              //just skip
            }
            else {
              $checkInStart = DB::select("SELECT A.*,B.U_NAMA FROM coll_check_in_start AS A INNER JOIN coll_user AS B ON A.CIS_COLL_U_ID = B.U_ID WHERE A.CIS_COLL_U_ID = ? AND A.BU_ID = ? AND A.CIS_LOKASI_LAT != '0' AND A.CIS_LOKASI_LNG != '0' ORDER BY A.CIS_ID ASC LIMIT 0,1", array($aData->{"BUD_COLL_U_ID"}, $buId));

              if(count($checkInStart) > 0) {
                if($checkInStart[0]->{"CIS_LOKASI_LAT"} != "0" && $checkInStart[0]->{"CIS_LOKASI_LNG"} != "0") {
                  $reply .= "<marker ";
                  $reply .= 'CUST_NAMA="CHECK-IN START" ';
                  $reply .= 'COLL_ID="'.parseToXML($aData->{"BUD_COLL_U_ID"}).'" ';
                  $reply .= 'COLL_NAMA="'.parseToXML($aData->{"U_NAMA"}) . '" ';
                  $reply .= 'COLL_STATUS="Check in start" ';
                  $reply .= 'COLL_STATUS_INFO="Check in start" ';
                  $reply .= 'COLL_POSISI_LAT="'.parseToXML($checkInStart[0]->{"CIS_LOKASI_LAT"}).'" ';
                  $reply .= 'COLL_POSISI_LNG="'.parseToXML($checkInStart[0]->{"CIS_LOKASI_LNG"}).'" ';
                  $reply .= 'COLL_STATUS_WAKTU="'.parseToXML(tglIndo($checkInStart[0]->{"CIS_WAKTU"},"SHORT")).'" ' ;
                  $reply .= 'BUD_ID="'.parseToXML($aData->{"BUD_ID"}).'" ' ;
                  $reply .= 'BU_ID="'.parseToXML($aData->{"BU_ID"}).'" ' ;
                  $reply .= 'TYPE="check-in_start" ';            
                  $reply .= '/>';
                  $reply .= "\n";

                  $lastWaktu = $checkInStart[0]->{"CIS_WAKTU"};
                }
              }

              $lastBatchUpload = $aData->{"BU_ID"};
              $lastCollector = $aData->{"BUD_COLL_U_ID"};
            }

            if(isset($lastWaktu) && trim($lastWaktu) !== "") {
              $arrDiff = dayDifference2($lastWaktu, $aData->{"BUD_STATUS_WAKTU"}, false);
              $selisihWaktu = /*$lastWaktu." s.d ".$aData->{"BUD_STATUS_WAKTU"}." : ".*/$arrDiff["DAY"]." hari, ".$arrDiff["MONTH"]." bulan, ".$arrDiff["HOUR"]." jam, ".$arrDiff["MINUTE"]." menit";
            }
            $lastWaktu = $aData->{"BUD_STATUS_WAKTU"};

            $reply .= "<marker ";
            $reply .= 'CUST_NAMA="'.parseToXML($aData->{"BUD_CUST_NAMA"}) . '" ';
            $reply .= 'COLL_ID="'.parseToXML($aData->{"BUD_COLL_U_ID"}).'" ';
            $reply .= 'COLL_NAMA="'.parseToXML($aData->{"U_NAMA"}) . '" ';
            $reply .= 'COLL_STATUS="'.parseToXML($aData->{"BUD_STATUS"}) . '" ';
            $reply .= 'COLL_STATUS_INFO="'.parseToXML(getReferenceInfo("STATUS_COLLECTION",$aData->{"BUD_STATUS"})) . '" ';
            $reply .= 'COLL_POSISI_LAT="'.parseToXML($aData->{"BUD_LOKASI_LAT"}).'" ';
            $reply .= 'COLL_POSISI_LNG="'.parseToXML($aData->{"BUD_LOKASI_LNG"}).'" ';
            $reply .= 'COLL_STATUS_WAKTU="'.parseToXML(tglIndo($aData->{"BUD_STATUS_WAKTU"},"SHORT")).'" ' ;
            $reply .= 'BUD_ID="'.parseToXML($aData->{"BUD_ID"}).'" ' ;
            $reply .= 'BU_ID="'.parseToXML($aData->{"BU_ID"}).'" ' ;
            if(isset($selisihWaktu)) {
              $reply .= 'SELISIH="'.parseToXML($selisihWaktu).'" ';
            }
            $reply .= 'TYPE="collector" ';            
            $reply .= '/>';
            $reply .= "\n";
          }
        }
      }

      if($reply != "")  $reply = "<markers>\n".$reply."</markers>";
      
      return Response::make($reply, '200')->header('Content-Type', 'text/xml');
    }
    else {
      Session::flush();
      return Response::make($reply, '200')->header('Content-Type', 'text/xml');
    } 
  }

  public function formlistCollector() {
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      $collectors = DB::table("coll_user")
        ->where("U_GROUP_ROLE", "GR_COLLECTOR")
        ->where("PRSH_ID", $userData->{"PRSH_ID"})
        ->get();

      return View::make("dashboard.collection.collector-formlist")  
        ->with("ctlUserData", $userData)
        ->with("ctlCollectors", $collectors)
        ->with("ctlNavMenu", "mCollData");
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    } 
  }

  public function submitCollector() {
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(isset($_FILES['collFile'])){
        $fileName = $_FILES['collFile']['name'];
        $fileSize = $_FILES['collFile']['size'];
        $fileTmp = $_FILES['collFile']['tmp_name'];
        $fileType = $_FILES['collFile']['type'];
        $a = explode(".", $_FILES["collFile"]["name"]);
        $fileExt = strtolower(end($a));

        $arrFileExt = array("xls","xlsx","XLS","XLSX");
        if(isset($fileName) && trim($fileName) != "") {
          if(in_array($fileExt,$arrFileExt)=== false)   return composeReply("ERROR","Harap pilih file Excel");
          if($fileSize > 2048000)                       return composeReply("ERROR","Harap pilih file Excel dengan ukuran max. 2 MB");
          
          $uploadFile = "uploads/collector-".createSlug($userId)."-".date("YmdHis").".".$fileExt;
          if(move_uploaded_file($fileTmp,$uploadFile) == TRUE) {    
            DB::beginTransaction();

            $objPHPExcel = PHPExcel_IOFactory::load($uploadFile);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

            $titles = $objWorksheet->rangeToArray('A1:' . $highestColumn . "1");
            $body = $objWorksheet->rangeToArray('A2:' . $highestColumn . $highestRow);
            $table = array();
            
            for ($row=0; $row <= $highestRow - 2; $row++) {
              $a = array();
              for ($column=0; $column <= $highestColumnIndex - 1; $column++) {
                if($column == 0 && $titles[0][$column] != "ID_COLLECTOR")   return composeReply("ERROR","Kolom ke-1 HARUS bernama ID_COLLECTOR");
                if($column == 1 && $titles[0][$column] != "NAMA")           return composeReply("ERROR","Kolom ke-2 HARUS bernama NAMA");
                
                $a[$titles[0][$column]] = $body[$row][$column];
              }
              $table[$row] = $a;
              if(isset($table[$row]) && trim($table[$row]["ID_COLLECTOR"]) !== "") {
                //cek dl apakah sdh ada user kolektor
                $kolektor = DB::table("coll_user")->where("U_ID",trim($table[$row]["ID_COLLECTOR"]))->first();
                if(count($kolektor) <= 0) {
                  $randomTelp = "99".randomDigits(10);
                  $randomEmail = strtolower(trim($table[$row]["ID_COLLECTOR"]))."@".strtolower($userData->{"PRSH_ID"});
                  DB::table("coll_user")->insert(array(
                    'U_ID' => trim($table[$row]["ID_COLLECTOR"]),
                    'U_PASSWORD' => getSetting("DEFAULT_PASSWORD"),
                    'U_PASSWORD_HASH' => md5(trim($table[$row]["ID_COLLECTOR"]).getSetting("DEFAULT_PASSWORD")),
                    'U_NAMA' => $table[$row]["NAMA"],
                    'U_GROUP_ROLE' => 'GR_COLLECTOR',
                    'U_TELPON' => $randomTelp,
                    'U_EMAIL' => $randomEmail,
                    'U_STATUS' => 'USER_ACTIVE',
                    'U_REGISTRASI_TGL' => date("Y-m-d H:i:s"),
                    'PRSH_ID' => $userData->{"PRSH_ID"}
                  ));
                }
              }
            }
            DB::commit();
            return composeReply("SUCCESS", "Data telah disimpan");
          }         
        }
        else {
          return composeReply("ERROR","Proses upload gagal (file upload tidak terdeteksi server)");
        }
      }
      else {
        return composeReply("ERROR","Harap sertakan file untuk diunggah");
      }
    }
    else {
      Session::flush();
      return composeReply("ERROR","Silahkan login terlebih dahulu");
    }
  }

  public function listDetailCollector() {
    if(null === Input::get("id") || trim(Input::get("id")) === "") {
      return Redirect::to("collection/collector");
    }

    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(null === Input::get("periode") || trim(Input::get("periode")) === "") {
        $periode = date("Y")."-".date("m");
      }
      else {
        $periode = Input::get("periode");
      }

      $arrPeriode = explode("-", $periode);
      $month = $arrPeriode[1];
      $year = $arrPeriode[0];

      $collectorData = DB::table("coll_user")->where("U_ID", Input::get("id"))->first();
      if(count($collectorData) <= 0)  return Redirect::to("collection/collector");

      $collRecords = DB::select("SELECT A.*, B.BU_TGL FROM coll_batch_upload_data AS A INNER JOIN coll_batch_upload AS B ON A.BU_ID = B.BU_ID WHERE A.BUD_COLL_U_ID = ? AND MONTH(BU_TGL) = ? AND YEAR(BU_TGL) = ?", array(Input::get("id"), $month, $year));
      
      return View::make("dashboard.collection.collector-detail-list") 
        ->with("ctlUserData", $userData)
        ->with("ctlFilterMonth",$month)
        ->with("ctlFilterYear", $year)
        ->with("ctlCollectorData", $collectorData)
        ->with("ctlCollRecords", $collRecords)
        ->with("ctlNavMenu", "mCollData");
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    }   
  }

  public function listLaporan() {
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();

      if(null === Input::get("periode") || trim(Input::get("periode")) === "") {
        $periode = date("Y")."-".date("m");
      }
      else {
        $periode = Input::get("periode");
      }

      $arrPeriode = explode("-", $periode);
      $month = $arrPeriode[1];
      $year = $arrPeriode[0];

      $collector = DB::table("coll_user")
        ->where("U_GROUP_ROLE", "GR_COLLECTOR")
        ->where("PRSH_ID", $userData->{"PRSH_ID"})
        ->get();
      
      return View::make("dashboard.collection.laporan-list")  
        ->with("ctlUserData", $userData)
        ->with("ctlFilterMonth",$month)
        ->with("ctlFilterYear", $year)
        ->with("ctlCollector", $collector)
        ->with("ctlNavMenu", "mCollLaporan");
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    }   
  }

  public function downloadLaporan() { 
    if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
      if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN'))) {
        Session::flush();
        return Redirect::to('login')->with('ctlError','Please login to access system'); 
      }

      $rptType = Input::get("tipe");
      if(!isset($rptType) || trim($rptType) === "") $rptType = "RPT_COLLECTING";

      $userId = Session::get('SESSION_USER_ID', '');      
      $userData = DB::table('coll_user')->where('U_ID',$userId)->first();  
      
      //****
      if($rptType == "RPT_COLLECTING") {
        $bln = date("m");
        $thn = date("Y");
        if(null !== Input::get("bln") && trim(Input::get("bln")) !== "")  $bln = Input::get("bln");
        if(null !== Input::get("thn") && trim(Input::get("thn")) !== "")  $thn = Input::get("thn");

        if(null !== Input::get("periode") && trim(Input::get("periode")) !== "") {
          $arrPeriode = explode("-", Input::get("periode"));
          $bln = $arrPeriode[1];
          $thn = $arrPeriode[0];
        }

        //formalitas
        $daysOfMonth = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
        $awal = date("Y")."-".date("m")."-01";
        $akhir = date("Y")."-".date("m")."-".$daysOfMonth;
        $collector = "ALL";
      }

      if($rptType == "RPT_COLLECTING_QUERY") {
        $bln = date("m"); //formalitas
        $thn = date("Y"); //formalitas

        $collector = "ALL";
        if(null !== Input::get("collector") && trim(Input::get("collector")) !== "")  $collector = Input::get("collector");

        $awal = date("Y")."-".date("m")."-01";
        $akhir = date("Y")."-".date("m")."-".getLastDate(date("m"), date("Y"));

        if(null !== Input::get("awal") && trim(Input::get("awal")) !== "")    $awal = Input::get("awal");
        if(null !== Input::get("akhir") && trim(Input::get("akhir")) !== "")  $akhir = Input::get("akhir");
      }

      Excel::create('Collection System Report', function($excel) use($rptType, $userData, $bln, $thn, $awal, $akhir, $collector) {
        $rptName = getReferenceInfo("REPORT_TYPE", $rptType);
        // Set the title
        $excel->setTitle('Collection System Report '.$thn."_".$bln);
        $excel->setCreator('Collection System')->setCompany($userData->{"PRSH_ID"});
        $excel->setDescription('Laporan hasil collecting');

        $excel->sheet('Sheet 1', function ($sheet) use($rptType, $userData, $bln, $thn, $awal, $akhir, $collector) {
          $sheet->setOrientation('landscape');
          //$sheet->fromArray($data, NULL, 'A4');       

          //$sheet->setCellValue('A1', "Tanggal Laporan");
          $sheet->setCellValue('A1', PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
          $sheet->getStyle('A1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
          
          $sheet->setCellValue('A2', "TANGGAL");
          $sheet->getColumnDimension('A')->setWidth(30);
          $sheet->setCellValue('B2', "NAMA COLLECTOR");
          $sheet->getColumnDimension('B')->setWidth(65);
          $sheet->setCellValue('C2', "ID COLLECTOR");
          $sheet->getColumnDimension('C')->setWidth(65);
          $sheet->setCellValue('D2', "NAMA CUSTOMER");
          $sheet->getColumnDimension('D')->setWidth(65);
          $sheet->setCellValue('E2', "ID CUSTOMER");
          $sheet->getColumnDimension('E')->setWidth(65);
          $sheet->setCellValue('F2', "ID PINJAMAN");
          $sheet->getColumnDimension('F')->setWidth(65);
          $sheet->setCellValue('G2', "PERIODE");
          $sheet->getColumnDimension('G')->setWidth(15);
          $sheet->setCellValue('H2', "MASA KREDIT");
          $sheet->getColumnDimension('H')->setWidth(35);
          $sheet->setCellValue('I2', "TGL KREDIT");
          $sheet->getColumnDimension('I')->setWidth(25);
          $sheet->setCellValue('J2', "TGL ANGSURAN");
          $sheet->getColumnDimension('J')->setWidth(25);
          $sheet->setCellValue('K2', "TGL JADWAL");
          $sheet->getColumnDimension('K')->setWidth(25);
          $sheet->setCellValue('L2', "POKOK");
          $sheet->getColumnDimension('L')->setWidth(35);
          $sheet->setCellValue('M2', "BUNGA");
          $sheet->getColumnDimension('M')->setWidth(35);
          $sheet->setCellValue('N2', "DENDA");
          $sheet->getColumnDimension('N')->setWidth(35);
          $sheet->setCellValue('O2', "TAGIHAN");
          $sheet->getColumnDimension('O')->setWidth(35);
          $sheet->setCellValue('P2', "PEMBAYARAN");
          $sheet->getColumnDimension('P')->setWidth(35);
          $sheet->setCellValue('Q2', "TGL BAYAR");
          $sheet->getColumnDimension('Q')->setWidth(35);
          $sheet->setCellValue('R2', "STATUS");
          $sheet->getColumnDimension('R')->setWidth(55);
          $sheet->setCellValue('S2', "K E T E R A N G A N");
          $sheet->getColumnDimension('S')->setWidth(75);

          $sheet->getStyle('A2:S2')->getFont()->setBold(true);

          $grandTotalBayar = 0;

          $xlsRow = 3;
          if($rptType == "RPT_COLLECTING") {
            $daysOfMonth = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);  
          }

          if($rptType == "RPT_COLLECTING_QUERY") {
            $arrDiff = dayDifference2($awal,$akhir,TRUE);
            $daysOfMonth = $arrDiff["DAY"] + 1;
          }
          
          for($i=1; $i<=$daysOfMonth; $i++) {
            if($rptType == "RPT_COLLECTING") {
              $aDate = $thn."-".$bln."-".$i;
              if(intval($aDate) < 10) $aDate = $thn."-".$bln."-0".$i;
            }

            if($rptType == "RPT_COLLECTING_QUERY") {
              $aDate = addDaysWithDate($awal,($i-1),'Y-m-d');
            }

            if($collector == "ALL") {
              $jadwal = DB::select("SELECT A.*,B.*,DATE(B.BUD_STATUS_WAKTU) AS TGL_STATUS,C.U_NAMA FROM coll_jadwal AS A INNER JOIN coll_batch_upload_data AS B ON A.BUD_ID = B.BUD_ID INNER JOIN coll_user AS C ON A.J_COLL_U_ID = C.U_ID WHERE A.J_TGL = ? AND A.PRSH_ID = ? ORDER BY A.J_TGL, A.J_COLL_U_ID, A.BU_ID, A.BUD_ID", array($aDate, $userData->{"PRSH_ID"}));
            }            
            else {
              $jadwal = DB::select("SELECT A.*,B.*,DATE(B.BUD_STATUS_WAKTU) AS TGL_STATUS,C.U_NAMA FROM coll_jadwal AS A INNER JOIN coll_batch_upload_data AS B ON A.BUD_ID = B.BUD_ID INNER JOIN coll_user AS C ON A.J_COLL_U_ID = C.U_ID WHERE A.J_TGL = ? AND A.PRSH_ID = ? AND A.J_COLL_U_ID = ? ORDER BY A.J_TGL, A.J_COLL_U_ID, A.BU_ID, A.BUD_ID", array($aDate, $userData->{"PRSH_ID"}, $collector));
            }

            $sheet->setCellValue('A'.$xlsRow, tglIndo($aDate,"SHORT"));
            $sheet->getStyle('A')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->getStyle('A'.$xlsRow)->getFont()->setBold(true);

            $lastCollector = "";
            $jmlPokokPerCollector = 0;
            $jmlBungaPerCollector = 0;
            $jmlDendaPerCollector = 0;
            $jmlTagihanPerCollector = 0;
            $jmlBayarPerCollector = 0;

            $jmlPokokPerTgl = 0;
            $jmlBungaPerTgl = 0;
            $jmlDendaPerTgl = 0;
            $jmlTagihanPerTgl = 0;
            $jmlBayarPerTgl = 0;
            foreach ($jadwal as $aData) {
              if($lastCollector == "")  $lastCollector = $aData->{"J_COLL_U_ID"};
              if($aData->{"J_COLL_U_ID"} != $lastCollector) {
                $sheet->mergeCells('A'.$xlsRow.':J'.$xlsRow); 
                $sheet->setCellValue('K'.$xlsRow, "T O T A L");
                $sheet->getStyle('K'.$xlsRow.':P'.$xlsRow)->getFont()->setBold(true);
                $sheet->setCellValue('L'.$xlsRow, $jmlPokokPerCollector);
                //$sheet->getStyle('L')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->setCellValue('M'.$xlsRow, $jmlBungaPerCollector);
                //$sheet->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->setCellValue('N'.$xlsRow, $jmlDendaPerCollector);
                //$sheet->getStyle('N')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->setCellValue('O'.$xlsRow, $jmlTagihanPerCollector);
                //$sheet->getStyle('O')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $sheet->setCellValue('P'.$xlsRow, $jmlBayarPerCollector);
                //$sheet->getStyle('P')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

                $jmlPokokPerCollector = 0;
                $jmlBungaPerCollector = 0;
                $jmlDendaPerCollector = 0;
                $jmlTagihanPerCollector = 0;
                $jmlBayarPerCollector = 0;
                $lastCollector = $aData->{"J_COLL_U_ID"};
                $xlsRow++;
              }

              $jmlPokokPerCollector += $aData->{"BUD_PINJ_POKOK"};
              $jmlBungaPerCollector += $aData->{"BUD_PINJ_BUNGA"};
              $jmlDendaPerCollector += $aData->{"BUD_PINJ_DENDA"};
              $jmlTagihanPerCollector += $aData->{"BUD_PINJ_JUMLAH"};
              $jmlBayarPerCollector += $aData->{"J_PINJ_JUMLAH_BAYAR"};

              $jmlPokokPerTgl += $aData->{"BUD_PINJ_POKOK"};
              $jmlBungaPerTgl += $aData->{"BUD_PINJ_BUNGA"};
              $jmlDendaPerTgl += $aData->{"BUD_PINJ_DENDA"};
              $jmlTagihanPerTgl += $aData->{"BUD_PINJ_JUMLAH"};
              $jmlBayarPerTgl += $aData->{"J_PINJ_JUMLAH_BAYAR"};

              $grandTotalBayar += $aData->{"J_PINJ_JUMLAH_BAYAR"};

              $sheet->setCellValue('B'.$xlsRow, $aData->{"U_NAMA"});
              //$sheet->getStyle('B')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('C'.$xlsRow, $aData->{"J_COLL_U_ID"});
              $sheet->getStyle('C')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('D'.$xlsRow, $aData->{"BUD_CUST_NAMA"});
              //$sheet->getStyle('D')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('E'.$xlsRow, $aData->{"BUD_CUST_ID"});
              $sheet->getStyle('E')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('F'.$xlsRow, $aData->{"BUD_PINJ_ID"});
              $sheet->getStyle('F')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('G'.$xlsRow, $aData->{"BUD_PINJ_PERIODE"});
              $sheet->getStyle('G')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('H'.$xlsRow, $aData->{"BUD_PINJ_MASA_KREDIT"});
              $sheet->getStyle('H')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('I'.$xlsRow, tglIndo($aData->{"BUD_PINJ_TGL_KREDIT"},"SHORT"));
              $sheet->getStyle('I')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('J'.$xlsRow, tglIndo($aData->{"BUD_PINJ_TGL_ANGS"},"SHORT"));
              $sheet->getStyle('J')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('K'.$xlsRow, tglIndo($aData->{"BUD_PINJ_TGL_JADWAL"},"SHORT"));
              $sheet->getStyle('K')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('L'.$xlsRow, $aData->{"BUD_PINJ_POKOK"});
              //$sheet->getStyle('L')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('M'.$xlsRow, $aData->{"BUD_PINJ_BUNGA"});
              //$sheet->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('N'.$xlsRow, $aData->{"BUD_PINJ_DENDA"});
              //$sheet->getStyle('N')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('O'.$xlsRow, $aData->{"BUD_PINJ_JUMLAH"});
              //$sheet->getStyle('O')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('P'.$xlsRow, $aData->{"J_PINJ_JUMLAH_BAYAR"});
              //$sheet->getStyle('P')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              
              if($aData->{"J_STATUS"} == "ST_BAYAR" || $aData->{"J_STATUS"} == "ST_BAYAR_PARSIAL") {
                $sheet->setCellValue('Q'.$xlsRow, tglIndo($aData->{"BUD_PINJ_TGL_BAYAR"},"SHORT"));
                $sheet->getStyle('Q')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              }
              else {
                $sheet->setCellValue('Q'.$xlsRow, "-");
              }
              
              $sheet->setCellValue('R'.$xlsRow, getReferenceInfo("STATUS_COLLECTION",$aData->{"J_STATUS"}));
              //$sheet->getStyle('R')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
              $sheet->setCellValue('S'.$xlsRow, $aData->{"BUD_KETERANGAN"});
              //$sheet->getStyle('S')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

              $xlsRow++;
            }
            //last row per collector
            $sheet->mergeCells('A'.$xlsRow.':J'.$xlsRow); 
            $sheet->setCellValue('K'.$xlsRow, "T O T A L");
            $sheet->getStyle('K'.$xlsRow.':P'.$xlsRow)->getFont()->setBold(true);
            $sheet->setCellValue('L'.$xlsRow, $jmlPokokPerCollector);
            //$sheet->getStyle('L')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('M'.$xlsRow, $jmlBungaPerCollector);
            //$sheet->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('N'.$xlsRow, $jmlDendaPerCollector);
            //$sheet->getStyle('N')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('O'.$xlsRow, $jmlTagihanPerCollector);
            //$sheet->getStyle('O')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('P'.$xlsRow, $jmlBayarPerCollector);
            //$sheet->getStyle('P')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $jmlPokokPerCollector = 0;
            $jmlBungaPerCollector = 0;
            $jmlDendaPerCollector = 0;
            $jmlTagihanPerCollector = 0;
            $jmlBayarPerCollector = 0;
            $lastCollector = "";
            $xlsRow++;
            $sheet->mergeCells('A'.$xlsRow.':I'.$xlsRow); 
            $sheet->setCellValue('J'.$xlsRow, "TOTAL PER ".strtoupper(tglIndo($aDate,"SHORT")));
            $sheet->getStyle('J'.$xlsRow.':P'.$xlsRow)->getFont()->setBold(true);
            $sheet->mergeCells('J'.$xlsRow.':K'.$xlsRow); 
            $sheet->setCellValue('L'.$xlsRow, $jmlPokokPerTgl);
            //$sheet->getStyle('L')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('M'.$xlsRow, $jmlBungaPerTgl);
            //$sheet->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('N'.$xlsRow, $jmlDendaPerTgl);
            //$sheet->getStyle('N')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('O'.$xlsRow, $jmlTagihanPerTgl);
            //$sheet->getStyle('O')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $sheet->setCellValue('P'.$xlsRow, $jmlBayarPerTgl);
            //$sheet->getStyle('P')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $xlsRow++;
          }
          $sheet->mergeCells('A'.$xlsRow.':O'.$xlsRow); 
          
          $sheet->getStyle("A".$xlsRow.":B".$xlsRow)->applyFromArray(array(
            'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
          ));
          
          $sheet->setCellValue('A'.$xlsRow, "T O T A L");          
          $sheet->getStyle('A'.$xlsRow.':P'.$xlsRow)->getFont()->setBold(true);
          $sheet->setCellValue('P'.$xlsRow, $grandTotalBayar);             

          // Set style for header row using alternative method
          $sheet->getStyle('A2:S2')->applyFromArray(
            array(
              'font'    => array(
                'bold'      => true
              ),
              'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              ),
              'borders' => array(
                'top'     => array(
                  'style' => PHPExcel_Style_Border::BORDER_THICK
                ),
                'bottom'     => array(
                  'style' => PHPExcel_Style_Border::BORDER_THICK
                )
              ),
              'fill' => array(
                'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                  'argb' => 'FFA0A0A0'
                ),
                'endcolor'   => array(
                  'argb' => 'FFFFFFFF'
                )
              )
            )
          );

          $sheet->getStyle('A2')->applyFromArray(
            array(
              'borders' => array(
                'left'     => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
            )
          );

          $sheet->getStyle('S2')->applyFromArray(
            array(
              'borders' => array(
                'right'     => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
            )
          );
        });

        $excel->setActiveSheetIndex(0);     
      })->download('xlsx');
    }
    else {
      Session::flush();
      return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
    }   
  }

  public function apiListJadwal() {
    if(null === Input::get("userId") || trim(Input::get("userId")) === "")          return composeReply2("ERROR", "Invalid user ID", "ACTION_LOGIN");
    if(null === Input::get("loginToken") || trim(Input::get("loginToken")) === "")  return composeReply2("ERROR", "Invalid login token", "ACTION_LOGIN");
    if(!isLoginValid(Input::get('userId'), Input::get('loginToken')))               return composeReply2("ERROR", "Invalid login token", "ACTION_LOGIN");

    $userData = DB::table("coll_user")->where("U_ID", Input::get("userId"))->first();
    if(count($userData) <= 0) return composeReply2("ERROR", "User ID tidak dikenal");

    if(null === Input::get("periode") || trim(Input::get("periode")) === "") {
      $periode = date("Y-m-d");
      $periode_dmY = date("d-m-Y");
    }
    else {
      $periode = Input::get("periode");
      $arrTgl = explode("-", $periode);
      $periode_dmY = $arrTgl[2]."-".$arrTgl[1]."-".$arrTgl[0];
    }
    
    //$latestBatchUpload = DB::select("SELECT BU_ID FROM coll_batch_upload WHERE PRSH_ID = ? AND BU_TGL <= ? ORDER BY BU_ID DESC LIMIT 0,1", array($userData->{"PRSH_ID"}, date("Y-m-d")));

    if(null === Input::get("status") || trim(Input::get("status")) === "") {
      //$collRecords = DB::select("SELECT X.* FROM (SELECT A.*,CURDATE() AS TGL_PERIODE, DATE_ADD(A.BUD_PINJ_TGL_JADWAL, INTERVAL A.BUD_PINJ_PERIODE DAY) AS TGL_BATAS FROM coll_batch_upload_data AS A WHERE A.BU_ID = ?) AS X WHERE X.TGL_PERIODE BETWEEN X.BUD_PINJ_TGL_JADWAL AND X.TGL_BATAS AND X.BUD_COLL_U_ID = ? ORDER BY X.BUD_ID", array($latestBatchUpload[0]->{"BU_ID"}, $userData->{"U_ID"}));
    
      $collRecords = DB::select("SELECT A.*,B.* FROM coll_jadwal AS A INNER JOIN coll_batch_upload_data AS B ON A.BUD_ID = B.BUD_ID WHERE A.J_TGL = ?  AND A.J_COLL_U_ID = ?", array($periode, $userData->{"U_ID"}));
    }
    else {
      //$collRecords = DB::select("SELECT X.* FROM (SELECT A.*,CURDATE() AS TGL_PERIODE, DATE_ADD(A.BUD_PINJ_TGL_JADWAL, INTERVAL A.BUD_PINJ_PERIODE DAY) AS TGL_BATAS FROM coll_batch_upload_data AS A WHERE A.BU_ID = ?) AS X WHERE X.TGL_PERIODE BETWEEN X.BUD_PINJ_TGL_JADWAL AND X.TGL_BATAS AND X.BUD_COLL_U_ID = ? AND X.BUD_STATUS = ? ORDER BY X.BUD_ID", array($latestBatchUpload[0]->{"BU_ID"}, $userData->{"U_ID"}, Input::get("status")));

      $collRecords = DB::select("SELECT A.*,B.* FROM coll_jadwal AS A INNER JOIN coll_batch_upload_data AS B ON A.BUD_ID = B.BUD_ID WHERE A.J_TGL = ?  AND A.J_COLL_U_ID = ? AND A.J_STATUS = ?", array($periode, $userData->{"U_ID"}, Input::get("status")));
    }

    foreach ($collRecords as $aData) {
      $aData->{"BUD_STATUS_INFO"} = getReferenceInfo("STATUS_COLLECTION", $aData->{"BUD_STATUS"});
      $aData->{"BUD_PINJ_TGL_KREDIT_FORMATTED"} = tglIndo($aData->{"BUD_PINJ_TGL_KREDIT"},"SHORT");
      $aData->{"BUD_PINJ_TGL_ANGS_FORMATTED"} = tglIndo($aData->{"BUD_PINJ_TGL_ANGS"},"SHORT");
      $aData->{"BUD_PINJ_TGL_JADWAL_FORMATTED"} = tglIndo($aData->{"BUD_PINJ_TGL_JADWAL"},"SHORT");
      $aData->{"BUD_PINJ_TGL_BAYAR_FORMATTED"} = tglIndo($aData->{"BUD_PINJ_TGL_BAYAR"},"SHORT");
      $aData->{"BUD_PINJ_POKOK_FORMATTED"} = number_format($aData->{"BUD_PINJ_POKOK"});
      $aData->{"BUD_PINJ_BUNGA_FORMATTED"} = number_format($aData->{"BUD_PINJ_BUNGA"});
      $aData->{"BUD_PINJ_DENDA_FORMATTED"} = number_format($aData->{"BUD_PINJ_DENDA"});
      $aData->{"BUD_PINJ_JUMLAH_FORMATTED"} = number_format($aData->{"BUD_PINJ_JUMLAH"});
      $aData->{"BUD_PINJ_JUMLAH_BAYAR_FORMATTED"} = number_format($aData->{"BUD_PINJ_JUMLAH_BAYAR"});
    }

    return composeReply2("SUCCESS", "Data jadwal ", $collRecords);
  }

  public function apiUpdateJadwal() {
    if(null === Input::get("userId") || trim(Input::get("userId")) === "")          return composeReply2("ERROR", "Invalid user ID");
    if(null === Input::get("loginToken") || trim(Input::get("loginToken")) === "")  return composeReply2("ERROR", "Invalid login token");
    if(!isLoginValid(Input::get('userId'), Input::get('loginToken')))               return composeReply2("ERROR", "Invalid login token");
    
    if(null === Input::get("status") || trim(Input::get("status")) === "")  return composeReply2("ERROR", "Status pembayaran harus diisi");

    if(Input::get("status") === "ST_JADWAL")  return composeReply2("ERROR", "Data tidak diproses karena status penagihan masih dalam penjadwalan");

    if(null === Input::get("budId") || trim(Input::get("budId")) === "")    return composeReply2("ERROR", "Invalid collection ID");
    $budData = DB::table("coll_batch_upload_data")->where("BUD_ID", Input::get("budId"))->first();
    if(count($budData) <= 0)  return composeReply2("ERROR", "Invalid collection ID");

    $userData = DB::table("coll_user")->where("U_ID", Input::get("userId"))->first();
    if(count($userData) <= 0) return composeReply2("ERROR","User ID tidak dikenal");

    $jmlBayar = 0;
    $tglBayar = "0000-00-00 00:00:00";
    $statusPenagihan = Input::get("status");
    if($statusPenagihan === "ST_BAYAR" || $statusPenagihan === "ST_BAYAR_PARSIAL") {
      if(null === Input::get("bayar") || trim(Input::get("bayar")) === "")  return composeReply2("ERROR", "Nominal pembayaran harus diisi");      
      $jmlBayar = Input::get("bayar");
      $tglBayar = date("Y-m-d H:i:s");

      if(!is_numeric($jmlBayar))    return composeReply2("ERROR", "Nominal pembayaran harus berupa bilangan");
      if(floatval($jmlBayar) <= 0)  return composeReply2("ERROR", "Masukkan nominal pembayaran yang benar");
      if(floatval($jmlBayar) < floatval($budData->{"BUD_PINJ_JUMLAH"})) {
        //return composeReply2("ERROR", "Nominal pembayaran HARUS sebesar Rp.".number_format($budData->{"BUD_PINJ_JUMLAH"}));
        $statusPenagihan = "ST_BAYAR_PARSIAL";
      }      
    }
    else { //PERLU DIPASTIKAN BGMN JIKA STATUS TIDAK BAYAR TP ADA NOMINAL PEMBAYARANNYA
      if(null !== Input::get("bayar") && trim(Input::get("bayar")) !== "")  {
        $jmlBayar = Input::get("bayar");  
        $tglBayar = date("Y-m-d H:i:s");

        if(!is_numeric($jmlBayar))    return composeReply2("ERROR", "Nominal pembayaran harus berupa bilangan");
        if(floatval($jmlBayar) <= 0)  return composeReply2("ERROR", "Masukkan nominal pembayaran yang benar");
        if(floatval($jmlBayar) < floatval($budData->{"BUD_PINJ_JUMLAH"})) {
          //return composeReply2("ERROR", "Nominal pembayaran HARUS sebesar Rp.".number_format($budData->{"BUD_PINJ_JUMLAH"}));
          $statusPenagihan = "ST_BAYAR_PARSIAL";
        }
        if(floatval($jmlBayar) >= floatval($budData->{"BUD_PINJ_JUMLAH"})) {
          $statusPenagihan = "ST_BAYAR";
        }
      }      
    }

    //cek data public system problem

    if(count($data > 0)) {
       $updateApi["DR_SUCCES"]->with("SUCCES", "Update Data berhasl,silahkan Cek status Penagihan Anda");
    } else {
       if(null === $data){
        $updateApi["DR_FAILED"]->with("ctlError", "Update in failed");
       }
    }

    if(isset($_FILES['uploadFile'])){
      $fileName = $_FILES['uploadFile']['name'];
      $fileSize = $_FILES['uploadFile']['size'];
      $fileTmp = $_FILES['uploadFile']['tmp_name'];
      $fileType = $_FILES['uploadFile']['type'];
      $a = explode(".", $_FILES["uploadFile"]["name"]);
      $fileExt = strtolower(end($a));

      $arrFileExt = array("jpg","jpeg","png","JPG","JPEG","PNG");
      if(isset($fileName) && trim($fileName) != "") {
        if(in_array($fileExt,$arrFileExt)=== false)   return composeReply2("ERROR","Harap pilih file JPG atau PNG");
        if($fileSize > 2048000)                       return composeReply2("ERROR","Harap pilih file JPG atau PNG dengan ukuran max. 2 MB");
        
        $uploadFile = "uploads/report-".$userData->{"U_ID"}."-".substr(md5(date("YmdHis")),0,10).".".$fileExt;
        if(move_uploaded_file($fileTmp,$uploadFile) == TRUE) {    
          $opr = DB::table("coll_batch_upload_data")->where("BUD_ID", Input::get("budId"))->update(array(
            'BUD_STATUS' => $statusPenagihan,
            'BUD_STATUS_WAKTU' => date("Y-m-d H:i:s"),
            'BUD_KETERANGAN' => Input::get("keterangan"),
            'BUD_PINJ_JUMLAH_BAYAR' => $jmlBayar,
            'BUD_PINJ_TGL_BAYAR' => $tglBayar,
            'BUD_LOKASI_LAT' => Input::get("latitude"),
            'BUD_LOKASI_LNG' => Input::get("longitude"),
            'BUD_IMG_PATH' => $uploadFile
          ));

          $opr = DB::table("coll_jadwal")
            ->where("BUD_ID", Input::get("budId"))
            ->where("J_TGL", "=", date("Y-m-d"))
            ->update(array(
              'J_STATUS' => $statusPenagihan,
              'J_PINJ_JUMLAH_BAYAR' => $jmlBayar
            ));

          if($statusPenagihan == "ST_BAYAR") {
            $nextStatus = "-";
          } 
          else {
            $nextStatus = "ST_JADWAL";
          }

          $opr2 = DB::table("coll_jadwal")
            ->where("BUD_ID", Input::get("budId"))
            ->where("J_TGL",">", date("Y-m-d"))
            ->update(array(
              'J_STATUS' => $nextStatus
            ));

          if($opr > 0) {
            return composeReply2("SUCCESS", "Data berhasil disimpan", array(
              'UPDATED_STATUS_INFO' => getReferenceInfo("STATUS_COLLECTION", Input::get("status")),
              'UPDATED_STATUS' => Input::get("status")
            ));
          }
          else {
            return composeReply2("ERROR", "Tidak terjadi perubahan data atau data gagal disimpan");
          }
        }         
        else {
          return composeReply2("ERROR","Proses upload gagal. Silahkan diulang kembali.");
        }
      }
      else {
        return composeReply2("ERROR","Proses upload gagal (file upload tidak terdeteksi server)");
      }
    }
    else {
      return composeReply2("ERROR","Harap sertakan file untuk diunggah");
    }
  }

  public function apiGetSummary() {
    if(null === Input::get("userId") || trim(Input::get("userId")) === "")          return composeReply2("ERROR", "Invalid user ID", "ACTION_LOGIN");
    if(null === Input::get("loginToken") || trim(Input::get("loginToken")) === "")  return composeReply2("ERROR", "Invalid login token", "ACTION_LOGIN");
    if(!isLoginValid(Input::get('userId'), Input::get('loginToken')))               return composeReply2("ERROR", "Invalid login token", "ACTION_LOGIN");
    
    $userData = DB::table("coll_user")->where("U_ID", Input::get("userId"))->first();

    $jmlStatusJadwal = 0;
    $jmlStatusBayar = 0;
    $jmlStatusBayarParsial = 0;
    $jmlStatusTdkBayar = 0;
    $jmlStatusTdkBertemu = 0;
    $nominalTarget = 0;
    $nominalBayar = 0;

    if(null === Input::get("periode") || trim(Input::get("periode")) === "") {
      $periode = date("Y-m-d");
      $periode_dmY = date("d-m-Y");
    }
    else {
      $periode = Input::get("periode");
      $arrTgl = explode("-", $periode);
      $periode_dmY = $arrTgl[2]."-".$arrTgl[1]."-".$arrTgl[0];
    }

    //$latestBatchUpload = DB::select("SELECT BU_ID FROM coll_batch_upload WHERE PRSH_ID = ? AND BU_TGL <= ? ORDER BY BU_ID DESC LIMIT 0,1", array($userData->{"PRSH_ID"}, date("Y-m-d")));
    
    //$collRecords = DB::select("SELECT X.* FROM (SELECT A.*,CURDATE() AS TGL_PERIODE, DATE_ADD(A.BUD_PINJ_TGL_JADWAL, INTERVAL A.BUD_PINJ_PERIODE DAY) AS TGL_BATAS FROM coll_batch_upload_data AS A WHERE A.BU_ID = ?) AS X WHERE X.TGL_PERIODE BETWEEN X.BUD_PINJ_TGL_JADWAL AND X.TGL_BATAS AND X.BUD_COLL_U_ID = ? ORDER BY X.BUD_COLL_U_ID", array($latestBatchUpload[0]->{"BU_ID"}, $userData->{"U_ID"}));

    $collRecords = DB::select("SELECT * FROM coll_jadwal WHERE J_TGL = ?  AND J_COLL_U_ID = ?", array($periode, $userData->{"U_ID"}));

    foreach ($collRecords as $aData) {
      //if($aData->{"BUD_STATUS"} == "ST_JADWAL") $jmlStatusJadwal++;
      //if($aData->{"BUD_STATUS"} == "ST_BAYAR")  $jmlStatusBayar++;
      //if($aData->{"BUD_STATUS"} == "ST_TIDAK_BAYAR")  $jmlStatusTdkBayar++;
      //if($aData->{"BUD_STATUS"} == "ST_TIDAK_DITEMUKAN")  $jmlStatusTdkBertemu++;
      //if($aData->{"BUD_STATUS"} == "ST_BAYAR_PARSIAL")  $jmlStatusBayarParsial++;

      if($aData->{"J_STATUS"} == "ST_JADWAL") $jmlStatusJadwal++;
      if($aData->{"J_STATUS"} == "ST_BAYAR")  $jmlStatusBayar++;
      if($aData->{"J_STATUS"} == "ST_TIDAK_BAYAR")  $jmlStatusTdkBayar++;
      if($aData->{"J_STATUS"} == "ST_TIDAK_DITEMUKAN")  $jmlStatusTdkBertemu++;
      if($aData->{"J_STATUS"} == "ST_BAYAR_PARSIAL")  $jmlStatusBayarParsial++;

      //$nominalTarget += floatval($aData->{"BUD_PINJ_JUMLAH"});
      //$nominalBayar += floatval($aData->{"BUD_PINJ_JUMLAH_BAYAR"});

      if($aData->{"J_STATUS"} != "-" && $aData->{"J_STATUS"} != "ST_BAYAR")  $nominalTarget += floatval($aData->{"J_PINJ_JUMLAH"});
      $nominalBayar += floatval($aData->{"J_PINJ_JUMLAH_BAYAR"});
    }

    return composeReply2("SUCCESS", "Summary data", array(
      /*'BATCH_UPLOAD_ID' => $latestBatchUpload[0]->{"BU_ID"},*/
      'SUMMARY_JADWAL' => $jmlStatusJadwal." orang",
      'JUMLAH_JADWAL' => $jmlStatusJadwal,
      'SUMMARY_BAYAR' => $jmlStatusBayar." orang",
      'JUMLAH_BAYAR' => $jmlStatusBayar,
      'SUMMARY_BAYAR_PARSIAL' => $jmlStatusBayarParsial." orang",
      'JUMLAH_BAYAR_PARSIAL' => $jmlStatusBayarParsial,
      'SUMMARY_TIDAK_BAYAR' => $jmlStatusTdkBayar." orang",
      'JUMLAH_TIDAK_BAYAR' => $jmlStatusTdkBayar,
      'SUMMARY_TIDAK_BERTEMU' => $jmlStatusTdkBertemu. " orang",
      'JUMLAH_TIDAK_BERTEMU' => $jmlStatusTdkBertemu,
      'SUMMARY_TARGET_BAYAR' => number_format($nominalTarget),
      'SUMMARY_REALISASI_BAYAR' => number_format($nominalBayar)
    ));
  }

  public function getReference($refCategory) {
    if(null === $refCategory || !isset($refCategory)) return composeReply2("ERROR", "Invalid category");

    $refData = DB::table("coll_referensi")->where("R_KATEGORI", $refCategory)
    ->orderBy('R_URUTAN', 'asc')
    ->get();

    return composeReply2("SUCCESS", "Referensi", $refData);
  }

  public function apiRegisterStartCheckIn() {
    if(null === Input::get("userId") || trim(Input::get("userId")) === "")          return composeReply2("ERROR", "Invalid user ID");
    if(null === Input::get("loginToken") || trim(Input::get("loginToken")) === "")  return composeReply2("ERROR", "Invalid login token");
    if(!isLoginValid(Input::get('userId'), Input::get('loginToken')))               return composeReply2("ERROR", "Invalid login token");

    if(null === Input::get("lat") || trim(Input::get("lat")) === "" || trim(Input::get("lat")) === "0")  return composeReply2("ERROR", "Latitude tidak terdeteksi");
    if(null === Input::get("lng") || trim(Input::get("lng")) === "" || trim(Input::get("lng")) === "0")  return composeReply2("ERROR", "Longitude tidak terdeteksi");
    
    $jadwal = DB::select("SELECT DISTINCT(BU_ID) AS BU_ID FROM coll_jadwal WHERE J_TGL = ? AND J_COLL_U_ID = ?", array(date("Y-m-d"), Input::get("userId")));
    
    if(count($jadwal) > 0) {
      foreach ($jadwal as $aData) {
        $cisId = DB::table("coll_check_in_start")->insertGetId(array(
          'CIS_WAKTU' => date("Y-m-d H:i:s"),
          'CIS_COLL_U_ID' => Input::get("userId"),
          'BU_ID' => $aData->{"BU_ID"},
          'CIS_LOKASI_LAT' => Input::get("lat"),
          'CIS_LOKASI_LNG' => Input::get("lng")
        ));
      }

      return composeReply2("SUCCESS", "Data check in start tersimpan");
    }
    else {
      return composeReply2("ERROR", "Tidak ada data jadwal per ".tglIndo(date("Y-m-d"), "SHORT")); 
    }
  }

  //dummy controller
  public function randomDataCollect() {
    $random = get("randomFunction");
    $collectId = get("collect_id");
    $rdm = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXZabcdefghijklmnopqrstufvwxz";

    if($random && $collectId) {
      rand($rdm)->with("SUCCES", "Nomer Verifikasi IN ". $rdm);
    }

    $verifikasi = array([$rdm => $random] == $collectId);

    if($verifikasi == 1) {
      echo "Berhasil Diverifikasi";
    } else {
      echo "Mohon maaf Nomer Verifikasi Ini tidak tersedia";
    }
  }
  
  public function cekRandomVerif() {
    $randomGet = Session::get(function::randomDataCollect);

    if($randomGet === 1){
      $verif = strtolower($randomGet)->with("SUCCES", "Check In Random Succes");
    } else {
      $verif = strtoupper($randomGet)->with("ctlError", "Check In Random Is empty");
    } else {
      $getMsg = "Check In Random Already Exist";
      echo $getMsg;
    }
  }

}
?>