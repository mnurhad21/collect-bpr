<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;  // <<< See here - no real class, only an alias
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
//use Input;
use Validator;
use Redirect;
use Session;
use Auth;


class UserController extends BaseController {
	public function redirectMissing() {
		Session::flush();
		return Redirect::to('login');
	}

	public function listUsers() {

		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {

			$userId = Session::get('SESSION_USER_ID', '');			
			$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			$refGroupRole = DB::table("coll_referensi")->where("R_KATEGORI","GROUP_ROLE")->get();

			$appUsers = DB::select("SELECT A.*,B.R_INFO,IFNULL(C.PRSH_NAMA,'-') AS PRSH_NAMA FROM coll_user AS A INNER JOIN coll_referensi AS B ON A.U_GROUP_ROLE = B.R_ID LEFT JOIN coll_perusahaan AS C ON A.PRSH_ID = C.PRSH_ID WHERE A.U_ID != ? AND B.R_KATEGORI = 'GROUP_ROLE'", array($userId));

			$prsh = DB::table("coll_perusahaan")->get();

			return view("dashboard.user-mgmt.user-formlist")	
				->with("ctlUserData", $userData)
				->with("ctlRefGroupRole", $refGroupRole)
				->with("ctlAppUsers", $appUsers)
				->with("ctlPrsh", $prsh)
				->with("ctlNavMenu", "mUserMgmt");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}
	}

	public function addUser() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			//$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("userId") || trim(Input::get("userId")) === "")	return composeReply("ERROR", "User ID harus diisi");
			$userId = preg_replace('/\s+/', '', Input::get("userId"));
			$userData = DB::table('coll_user')->where('U_ID', $userId)->first();
			if(count($userData) > 0)	return composeReply("ERROR", "User ID sudah digunakan");

			if(null === Input::get("userEmail") || trim(Input::get("userEmail")) === "")	return composeReply("ERROR", "Email harus diisi");
			$userEmail = preg_replace('/\s+/', '', Input::get("userEmail"));
			$cek = DB::table('coll_user')->where('U_EMAIL', $userEmail)->first();
			if(count($cek) > 0)	return composeReply("ERROR", "Email sudah digunakan");

			if(null === Input::get("userPonsel") || trim(Input::get("userPonsel")) === "")	return composeReply("ERROR", "Nomor ponsel harus diisi");
			if(!is_numeric(Input::get("userPonsel")))	return composeReply("ERROR", "Isikan ponsel dengan format 08xxx");
			$userPonsel = formatPonsel(preg_replace('/\s+/', '', Input::get("userPonsel")), "0");
			$cek = DB::table('coll_user')->where('U_TELPON', $userPonsel)->first();
			if(count($cek) > 0)	return composeReply("ERROR", "Nomor ponsel sudah digunakan");

			if(null === Input::get("userPrsh") || trim(Input::get("userPrsh")) === "")	return composeReply("ERROR", "Perusahaan harus diisi");
			if(trim(Input::get("userPrsh") != "-")) {
				$prshData = DB::table("coll_perusahaan")->where("PRSH_ID", Input::get("userPrsh"))->first();
				if(count($prshData) <= 0)	return composeReply("ERROR", "Perusahan tidak dikenal");
			}

			if(null === Input::get("userNama") || trim(Input::get("userNama")) === "")		return composeReply("ERROR", "Nama user harus diisi");				
			if(null === Input::get("userGroup") || trim(Input::get("userGroup")) === "")	return composeReply("ERROR", "Group role user harus diisi");				
			if(null === Input::get("userPass") || trim(Input::get("userPass")) === "")		return composeReply("ERROR", "Password harus diisi minimal 6 karakter");
			if(strlen(Input::get("userPass")) < 6)	return composeReply("ERROR", "Password harus diisi minimal 6 karakter");

			//TO DO 
			//tambah rule jika non perusahaan hanya boleh input administrator
			if(trim(Input::get("userPrsh")) === "-" && Input::get("userGroup") != "GR_ADMINISTRATOR")	return composeReply("ERROR", "User non-perusahaan hanya bisa diset sebagai anggota grup ".getReferenceInfo("GROUP_ROLE", "GR_ADMINISTRATOR"));

			//jika user perusahaan, hanya boleh diset sbg spv atau coll
			if(trim(Input::get("userPrsh")) !== "-" && Input::get("userGroup") === "GR_ADMINISTRATOR")	return composeReply("ERROR", "User perusahaan hanya bisa diset sebagai anggota grup ".getReferenceInfo("GROUP_ROLE", "GR_SUPERVISOR")." atau grup ".getReferenceInfo("GROUP_ROLE", "GR_COLLECTOR"));

			$opr = DB::table("coll_user")->insert(array(
				'U_ID' => $userId,
				'U_NAMA' => Input::get("userNama"),
				'U_GROUP_ROLE' => Input::get("userGroup"),
				'U_TELPON' => $userPonsel,
				'U_EMAIL' => $userEmail,
				'U_PASSWORD' => Input::get("userPass"),
				'U_PASSWORD_HASH' => md5($userId.Input::get("userPass")),
				'U_STATUS' => 'USER_ACTIVE',
				'PRSH_ID' => Input::get("userPrsh")
			));
			
			return composeReply("SUCCESS", "Data user telah disimpan");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}	
	}

	public function updateUser() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			//$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("userId") || trim(Input::get("userId")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			
			$userData = DB::table('coll_user')->where('U_ID',Input::get("userId"))->first();
			if(count($userData) <= 0)	return composeReply("ERROR", "User tidak dikenal");

			if(null === Input::get("userEmail") || trim(Input::get("userEmail")) === "")	return composeReply("ERROR", "Email harus diisi");
			$userEmail = preg_replace('/\s+/', '', Input::get("userEmail"));
			if($userData->{"U_EMAIL"} !== $userEmail) {
				$cek = DB::table('coll_user')->where('U_EMAIL', $userEmail)->first();
				if(count($cek) > 0)	return composeReply("ERROR", "Email sudah digunakan");
			}

			if(null === Input::get("userPonsel") || trim(Input::get("userPonsel")) === "")	return composeReply("ERROR", "Nomor ponsel harus diisi");
			if(!is_numeric(Input::get("userPonsel")))	return composeReply("ERROR", "Isikan ponsel dengan format 08xxx");
			$userPonsel = formatPonsel(preg_replace('/\s+/', '', Input::get("userPonsel")), "0");
			if($userData->{"U_TELPON"} !== $userPonsel) {
				$cek = DB::table('coll_user')->where('U_TELPON', $userPonsel)->first();
				if(count($cek) > 0)	return composeReply("ERROR", "Nomor ponsel sudah digunakan");
			}

			if(null === Input::get("userPrsh") || trim(Input::get("userPrsh")) === "")	return composeReply("ERROR", "Perusahaan harus diisi");
			if(trim(Input::get("userPrsh") != "-")) {
				$prshData = DB::table("coll_perusahaan")->where("PRSH_ID", Input::get("userPrsh"))->first();
				if(count($prshData) <= 0)	return composeReply("ERROR", "Perusahan tidak dikenal");
			}

			if(null === Input::get("userNama") || trim(Input::get("userNama")) === "")		return composeReply("ERROR", "Nama user harus diisi");				
			if(null === Input::get("userGroup") || trim(Input::get("userGroup")) === "")	return composeReply("ERROR", "Group role user harus diisi");	

			//TO DO 
			//tambah rule jika non perusahaan hanya boleh input administrator
			if(trim(Input::get("userPrsh")) === "-" && Input::get("userGroup") != "GR_ADMINISTRATOR")	return composeReply("ERROR", "User non-perusahaan hanya bisa diset sebagai anggota grup ".getReferenceInfo("GROUP_ROLE", "GR_ADMINISTRATOR"));

			//jika user perusahaan, hanya boleh diset sbg spv atau coll
			if(trim(Input::get("userPrsh")) !== "-" && Input::get("userGroup") === "GR_ADMINISTRATOR")	return composeReply("ERROR", "User perusahaan hanya bisa diset sebagai anggota grup ".getReferenceInfo("GROUP_ROLE", "GR_SUPERVISOR")." atau grup ".getReferenceInfo("GROUP_ROLE", "GR_COLLECTOR"));
			
			DB::table("coll_user")->where("U_ID", Input::get("userId"))->update(array(
				'U_NAMA' => Input::get("userNama"),
				'U_GROUP_ROLE' => Input::get("userGroup"),
				'U_TELPON' => $userPonsel,
				'U_EMAIL' => $userEmail,
				'PRSH_ID' => Input::get("userPrsh")
			));
			
			return composeReply("SUCCESS", "Perubahan data user telah disimpan");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}	
	}

	public function deleteUser() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			//$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			$userData = DB::table('coll_user')->where('U_ID',Input::get("id"))->first();
			if(count($userData) <= 0)	return composeReply("ERROR", "User tidak dikenal");

			$batchData = DB::table("coll_batch_upload_data")->where("BUD_COLL_U_ID", $userData->{"U_ID"})->get();
			if(count($batchData) > 0)	return composeReply("ERROR", "Data user tidak bisa dihapus karena terdapat riwayat penagihan oleh user tsb");

			DB::table("coll_user")->where("U_ID", Input::get("id"))->delete();
			
			return composeReply("SUCCESS", "Data telah dihapus");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}		
	}

	public function resetUserPassword() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			//$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			$userData = DB::table('coll_user')->where('U_ID',Input::get("id"))->first();
			if(count($userData) <= 0)	return composeReply("ERROR", "User tidak dikenal");
			
			$defaultPassword = getSetting("DEFAULT_PASSWORD");

			DB::table("coll_user")->where("U_ID", Input::get("id"))->update(array(
				'U_PASSWORD' => $defaultPassword,
				'U_PASSWORD_HASH' => md5($userData->{"U_ID"}.$defaultPassword),
				'U_LOGIN_TOKEN' => '-'
			));
			
			return composeReply("SUCCESS", "Perubahan data telah disimpan");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}		
	}

	public function getUserData() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			//$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");

			$userData = DB::table('coll_user')->where('U_ID',Input::get("id"))->first();
			if(count($userData) <= 0)	return composeReply("ERROR", "User tidak terdaftar");

			return composeReply("SUCCESS", "User data", $userData);
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}	
	}

	public function updateUserStatus() {
		if (Session::has('SESSION_USER_ID') && Session::has('SESSION_LOGIN_TOKEN')) {
			if(!isLoginValid(Session::get('SESSION_USER_ID'), Session::get('SESSION_LOGIN_TOKEN')))	{
				Session::flush();
				return Redirect::to('login')->with('ctlError','Please login to access system');	
			}

			$userId = Session::get('SESSION_USER_ID', '');			
			//$userData = DB::table('coll_user')->where('U_ID',$userId)->first();

			if(null === Input::get("id") || trim(Input::get("id")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			$userData = DB::table('coll_user')->where('U_ID',Input::get("id"))->first();
			if(count($userData) <= 0)	return composeReply("ERROR", "User tidak dikenal");

			if(null === Input::get("userStatus") || trim(Input::get("userStatus")) === "")	return composeReply("ERROR", "Parameter tidak lengkap");
			if(strtoupper(trim(Input::get("userStatus"))) == "AKTIF") {
				$status = "USER_ACTIVE";
			}
			elseif(strtoupper(trim(Input::get("userStatus"))) == "NON_AKTIF") {
				$status = "USER_INACTIVE";
			}
			else {
				return composeReply("ERROR", "Parameter tidak sesuai");
			}

			DB::table("coll_user")->where("U_ID", Input::get("id"))->update(array(
				'U_STATUS' => $status,
				'U_LOGIN_TOKEN' => '-'
			));
			
			return composeReply("SUCCESS", "Perubahan data telah disimpan");
		}
		else {
			Session::flush();
			return Redirect::to('login')->with('ctlError','Harap login terlebih dahulu');
		}		
	}
}
?>