@extends('dashboard.layout-dashboard')
@section('content')
<?php include_once('../application/helpers/helper.php'); ?>
<div class="page-header">
  <div class="page-header-content">
    <div class="page-title">
      <h4><i class="icon-arrow-left52 position-left"></i> Profile</h4>
    </div>
    <!--
    <div class="heading-elements">
      <div class="heading-btn-group">
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
      </div>
    </div>
    //-->
  </div>

  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li><a href="<?php echo asset_url(); ?>"><i class="icon-home2 position-left"></i> Beranda</a></li>
      <li><i class="icon-menu position-left"></i> General</a></li>
      <li class="active"><i class="icon-profile"></i> Profil</li>
    </ul>
    <!--
    <ul class="breadcrumb-elements">
      <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="icon-gear position-left"></i>
          Settings
          <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
          <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
          <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
          <li class="divider"></li>
          <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
        </ul>
      </li>
    </ul>
    //-->
  </div>
</div>

<div class="content">
  <form class="form-horizontal" method="post" id="formData" name="formData"> 
    <div class="panel panel-flat">
      <div class="panel-heading">
        <h5 class="panel-title text-semibold" style="color:rgb(51, 150, 213);">Profil</h5>
      </div>

      <div class="panel-body">
        <div class="form-group">
          <label class="control-label col-lg-3">User ID</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon text-semibold"><i class="icon-user"></i></span>
              <input type="text" id="userID" name="userID" class="form-control text-semibold" value="<?php echo $ctlUserData->{"U_ID"}; ?>" readonly>
            </div>                        
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Nama</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-profile"></i></span>
              <input type="text" id="nama" name="nama" class="form-control" value="<?php echo $ctlUserData->{"U_NAMA"}; ?>">
            </div>                        
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Email</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-mention"></i></span>
              <input type="text" id="email" name="email" class="form-control" value="<?php echo $ctlUserData->{"U_EMAIL"}; ?>">
            </div>                        
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">No. Ponsel</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-phone2"></i></span>
              <input type="text" id="ponsel" name="ponsel" class="form-control" value="<?php echo $ctlUserData->{"U_TELPON"}; ?>">
            </div>                        
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Password Lama</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
              <input type="password" id="passwordLama" name="passwordLama" class="form-control" >
            </div>                                    
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Password Baru</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
              <input type="password" id="passwordBaru" name="passwordBaru" class="form-control" >
            </div>                                                
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Konfirmasi Password Baru</label>
          <div class="col-lg-5">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
              <input type="password" id="passwordKonfirmasi" name="passwordKonfirmasi" class="form-control" >
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-6 control-label text-semibold">&nbsp;</label>            
          <div class="col-lg-2">
            <button type="button" class="btn bg-green btn-block btn-labeled" onClick="saveData()"><b><i class="fa fa-check position-right"></i></b> Simpan</button>
          </div>            
        </div>
      </div>
    </div>
  </form>   
  <div class="footer text-muted"></div>

  <div class="footer text-muted"></div>
</div>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/pages/form_select2.js"></script>
<script type="text/javascript">
  // Select with search
  $('.select-search').select2();
</script>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/jquery.form.js"></script>
<script type="text/javascript">
  $(function() {
    // Table setup
    // ------------------------------
    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,        
        /*
        columnDefs: [{ 
            orderable: false,
            width: '80px',
            targets: [ 3 ]
        }],*/
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Cari &nbsp;</span> _INPUT_',
            lengthMenu: '<span>Tampil &nbsp;</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Datatable with saving state
    $('.datatable-basic').DataTable({
        stateSave: true,
        "order": [[ 0, "desc" ]]
    });

    // External table additions
    // ------------------------------
    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Kata kunci...');

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });    
  });

  $(".first-input").focus();

  var saveData = function(){
    var id = "<?php echo $ctlUserData->{'U_ID'}; ?>";
    var nama = $("#nama").val();
    var email = $("#email").val();
    var ponsel = $("#ponsel").val();
    var passwordLama = $("#passwordLama").val();
    var passwordBaru = $("#passwordBaru").val();
    var passwordKonfirmasi = $("#passwordKonfirmasi").val();
    
    createOverlay("Proses...");
    $.ajax({  
      type  : "PUT",
      url   : "<?php echo asset_url(); ?>/profil",
      data  : "id=" + id + "&nama=" + encodeURI(nama) + "&email=" + encodeURI(email) + "&ponsel=" + encodeURI(ponsel) + "&passwordLama=" + passwordLama + "&passwordBaru=" + passwordBaru + "&passwordKonfirmasi=" + passwordKonfirmasi,
      success : function(result) { 
        gOverlay.hide();
        var data = JSON.parse(result);

        if(data["STATUS"] == "SUCCESS") {            
          toastr.success(data["MESSAGE"]);
          setTimeout(function(){ 
            window.location = "<?php echo asset_url(); ?>/profil";
          }, 500);              
        }
        else {
          //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
          //toastr.error(data["MESSAGE"]);
          swal({
            title: "GAGAL",
            text: data["MESSAGE"],
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "OK",
            closeOnConfirm: false,
            html: true
          },
          function(){
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/profil";
            }, 500);              
          });
        }
      },
      error : function(error) {   
        gOverlay.hide();
        alert("Gangguan pada server/jaringan\r\n" + error);
      }
    });    
  }
</script>
@stop