@extends('dashboard.layout-dashboard')
@section('content')
<?php include_once('../application/helpers/helper.php'); ?>
<div class="page-header">
  <div class="page-header-content">
    <div class="page-title">
      <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Collector</span></h4>
    </div>
    <!--
    <div class="heading-elements">
      <div class="heading-btn-group">
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
      </div>
    </div>
    //-->
  </div>

  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li><a href="<?php echo asset_url(); ?>"><i class="icon-home2 position-left"></i> Beranda</a></li>
      <li><i class="icon-menu position-left"></i> Collection</a></li>
      <li class="active"><i class="icon-users4"></i> Data Collector</li>
    </ul>
    <!--
    <ul class="breadcrumb-elements">
      <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="icon-gear position-left"></i>
          Settings
          <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
          <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
          <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
          <li class="divider"></li>
          <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
        </ul>
      </li>
    </ul>
    //-->
  </div>
</div>

<div class="content">
  <div class="panel panel-flat border-top-primary">
    <div class="panel-heading">
      <h5 class="panel-title text-semibold text-primary">Unggah File Data Collector</h5>
      <div class="heading-elements">
        <ul class="icons-list">  
          <li><a href="<?php echo asset_url(); ?>/assets/documents/sample_collector.xlsx"><i class="  icon-download4"></i> Unduh File Contoh</a></li>
          <li><a data-action="collapse"></a></li>
        </ul>
      </div>
      <!--
      <div class="heading-elements">
        <ul class="icons-list">
          <li><a data-action="collapse"></a></li>
          <li><a data-action="reload"></a></li>
          <li><a data-action="close"></a></li>
        </ul>
      </div>
      //-->
    </div>

    <div class="panel-body">
      <p class="content-group">
        Unggah file Excel berisi petugas collector.
      </p>

      <p class="content-group">
        Harap pastikan isi dan format file sudah benar. Setelah diunggah, petugas akan bisa login menggunakan user ID dan password default.
      </p>

      <form class="form-horizontal" enctype="multipart/form-data" method="post" id="formUploadData" name="formUploadData" action="<?php echo asset_url(); ?>/collection/collector">
        <div class="form-group">
          <!--<label class="col-lg-2 control-label text-semibold">File Data Format Excel</label>-->
          <div class="col-lg-4">
            <input type="file" class="file-input" data-show-preview="false" data-show-upload="false" id="collFile" name="collFile" placeholder="File format Excel...">
          </div>            
        </div>

        <div class="form-group">
          <!--label class="col-lg-2 control-label text-semibold">&nbsp;</label-->            
          <div class="col-lg-4">
            <button onClick="saveContent()" class="btn bg-blue-theme btn-block">Unggah File<i class="fa fa-upload position-right"></i></button>
          </div>            
        </div>
      </form>
      <!--
      <div class="form-group">
        <label class="col-lg-2 control-label text-semibold">File Data Format .xls/.xlsx</label>
        <div class="col-lg-4">
          <input type="file" class="file-input" multiple="multiple" data-show-upload="false" name="input001_multi[]" id="input001_multi">
          <span class="help-block">Anda bisa memilih beberapa file sekaligus untuk diunggah.</span>
          <button type="submit" class="btn btn-primary" id="btnSubmit" disabled><i class="icon-arrow-right14"></i> Simpan</button>
        </div>
      </div>
      //-->     
    </div>
  </div>
   
  <div class="panel panel-flat border-top-primary">
    <form class="form-horizontal" method="post" id="formData" name="formData">
      <div class="panel-heading">
        <h5 class="panel-title text-semibold" style="color:rgb(51, 150, 213);">Tambah Collector</h5>
        <div class="heading-elements">
          <ul class="icons-list">  
            <li><a data-action="collapse"></a></li>
          </ul>
        </div>
      </div>

      <div class="panel-body">
        <input type="hidden" id="userPrsh" value="<?php echo $ctlUserData->{"PRSH_ID"}; ?>">
        <input type="hidden" id="userGroup" value="GR_COLLECTOR"> 
        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">User ID</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-user"></i></span>
              <input type="text" class="form-control first-input" id="userId" name="userId" placeholder="User ID...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Password</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
              <input type="password" class="form-control" id="userPass" name="userPass" placeholder="Password...">
            </div>                        
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Nama</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-profile"></i></span>
              <input type="text" class="form-control" id="userNama" name="userNama" placeholder="Nama user...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">No.Ponsel</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-phone2"></i></span>
              <input type="text" class="form-control" id="userPonsel" name="userPonsel" placeholder="Nomor ponsel user...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Email</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-mention"></i></span>
              <input type="text" class="form-control" id="userEmail" name="userEmail" placeholder="Email user...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-4 control-label text-semibold">&nbsp;</label>            
          <div class="col-lg-2">
            <button type="button" class="btn bg-green btn-block btn-labeled" onClick="saveData()"><b><i class="fa fa-check position-right"></i></b> Simpan</button>
          </div>            
        </div>
      </div>
    </form>
  </div>

  <div class="panel panel-flat border-top-primary">
    <div class="panel-heading">
      <h5 class="panel-title text-semibold" style="color:rgb(51, 150, 213);">Data Collector</h5>
      <div class="heading-elements">
        <ul class="icons-list">  
          <li><a data-action="collapse"></a></li>
        </ul>
      </div>
    </div>
    <div class="panel-body">
      <table class="table datatable-basic">
        <thead>
          <tr>
            <th style="text-align:center" width="18%">User ID</th>
            <th style="text-align:center" width="20%">Nama</th>
            <th style="text-align:center" width="15%">Status</th>
            <th style="text-align:center" width="15%">Group Role</th>
            <th style="text-align:center" width="13%">Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(isset($ctlCollectors) && count($ctlCollectors) > 0) {
            foreach ($ctlCollectors as $aData) {
              if($aData->{"U_STATUS"} == "USER_ACTIVE")    $btnClass = "btn-success";
              if($aData->{"U_STATUS"} == "USER_INACTIVE")  $btnClass = "btn-danger";
              ?>
              <tr>
                <td><b><?php echo $aData->{"U_ID"}; ?></b></td>
                <td><?php echo $aData->{"U_NAMA"}; ?></td>
                <td style="text-align:center">
                  <div id="divAktif_<?php echo $aData->{"U_ID"}; ?>_Label" style="display:none"><span class="clickable" data-id="<?php echo $aData->{"U_ID"}; ?>"><?php echo getReferenceInfo("USER_STATUS",$aData->{"U_STATUS"}); ?></span></div>
                  <div id="divAktif_<?php echo $aData->{"U_ID"}; ?>_Edit" style="display:block">
                    <div class="btn-group">
                      <button type="button" class="btn <?php echo $btnClass; ?> btn-icon dropdown-toggle" data-toggle="dropdown">
                        <?php echo strtoupper(getReferenceInfo("USER_STATUS",$aData->{"U_STATUS"})); ?> &nbsp;<span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="javascript:setStatus('AKTIF','<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-check"></i> Aktif</a></li>
                        <li><a href="javascript:setStatus('NON_AKTIF','<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-remove"></i> Non-Aktif</a></li>
                        <!--li><a href="javascript:setStatus('SUSPEND','<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-ban"></i> Suspend</a></li-->
                      </ul>
                    </div>
                  </div>
                </td>
                <td style="text-align:center"><?php echo getReferenceInfo("GROUP_ROLE",$aData->{"U_GROUP_ROLE"}); ?></td>
                <td style="text-align:center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-icon dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="javascript:showDetail('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-ellipsis-h"></i> Detil</a></li>
                        <li><a href="javascript:editData('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-edit"></i> Ubah</a></li>
                        <li><a href="javascript:resetPassword('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-retweet"></i> Reset Password</a></li>
                        <li><a href="javascript:deleteData('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-remove"></i> Hapus</a></li>
                      </ul>
                    </div>
                  </td>
              </tr>      
              <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>  

<div id="mdlUser_Edit" class="modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title"><i class="icon-user"></i> &nbsp; Edit User</h6>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" method="post" id="formCost" name="formCost" action="">   
          <input type="hidden" id="userPrshEdit" value="<?php echo $ctlUserData->{"PRSH_ID"}; ?>"> 
          <input type="hidden" id="userGroupEdit" value="GR_COLLECTOR"> 
          <div class="form-group">
            <label class="col-lg-4 control-label text-semibold">User ID</label>
            <div class="col-lg-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-user"></i></span>
                <input type="text" id="userIdEdit" class="form-control" readonly>
              </div>
            </div>            
          </div>

          <div class="form-group">
            <label class="col-lg-4 control-label text-semibold">Nama</label>
            <div class="col-lg-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-profile"></i></span>
                <input type="text" id="userNamaEdit" class="form-control">
              </div>
            </div>            
          </div>

          <div class="form-group">
            <label class="col-lg-4 control-label text-semibold">No.Ponsel</label>
            <div class="col-lg-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-phone2"></i></span>
                <input type="text" id="userPonselEdit" class="form-control">
              </div>
            </div>            
          </div>

          <div class="form-group">
            <label class="col-lg-4 control-label text-semibold">Email</label>
            <div class="col-lg-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-mention"></i></span>
                <input type="text" class="form-control" id="userEmailEdit" name="userEmailEdit" placeholder="Email user...">
              </div>            
            </div>
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-warning btn-labeled btn-xs" data-dismiss="modal"><b><i class="icon-cross3"></i></b> Tutup </button>
        <button type="button" class="btn btn-primary btn-labeled btn-xs" onClick="updateData();" id="btnUpdateUser" data-user-id=""><b><i class="icon-checkmark2"></i></b> Simpan </button>
      </div>
    </div>
  </div>
</div>

<div class="footer text-muted"></div>

<!-- Theme JS files -->
<script type="text/javascript">
  // Select with search
  //$('.select').select2();
</script>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/jquery.form.js"></script>
<script type="text/javascript">
  $(function() {
    // Table setup
    // ------------------------------
    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,        
        /*
        columnDefs: [{ 
            orderable: false,
            width: '80px',
            targets: [ 3 ]
        }],*/
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search &nbsp;</span> _INPUT_',
            lengthMenu: '<span>Show &nbsp;</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Datatable with saving state
    $('.datatable-basic').DataTable({
        stateSave: true,
        "order": [[ 0, "desc" ]]/*,
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   {
          leftColumns: 1,
          rightColumns: 1
        }*/
    });

    // External table additions
    // ------------------------------
    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Keyword...');

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    }); 

    $(".datepicker").datepicker({ 
      dateFormat: 'dd-mm-yy' 
    });

    $('.pickatime-hidden').pickatime({
      formatSubmit: 'HH:i',
      hiddenName: true
    });   
  });

  var gBusy = false;

  function saveContent() {    
    if(!gBusy) {
      gBusy = true;
      //- content action form
      var optFormContentUpdate = {
        beforeSubmit  : function(e) {
          $(".btn").attr("disabled","");
          createOverlay("Proses...");
          return true;
        }, 
        resetForm : false,
        success : function(result) {
          gBusy = false;
          gOverlay.hide();
          var data = JSON.parse(result);
          $(".btn").removeAttr("disabled");
          if(data["STATUS"] == "SUCCESS") {
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/collection/collector";
            }, 500);            
            /*
            swal({
              title: "SUKSES",
              text: data["MESSAGE"],
              type: "success",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);
            });
            */
          }
          else {
            //toastr.error(data["MESSAGE"]);            
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: true,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);              
            });
          }        
          //$(".btn").removeAttr("disabled");
          //window.location = "<?php echo asset_url(); ?>/collection/collector";
        },
        error : function(e) {
          gBusy = false;
          gOverlay.hide();

          toastr.error("Proses <b>GAGAL</b>");     
          $(".btn").removeAttr("disabled");   
          window.location = "<?php echo asset_url(); ?>/collection/collector";
        }
      };  
      $('#formUploadData').submit(function() {
        $(this).ajaxSubmit(optFormContentUpdate); 
        // return false to prevent standard browser submit and page navigation
        return false;
      });
    }
  }

  function showDetail(uId) {
    window.location = "<?php echo asset_url(); ?>/collection/collector/detail?id=" + uId;
  }

  var setStatus = function(userStatus,slug){
    swal({
      title: "Konfirmasi",
      text: "Ubah status user ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/user/update-status",
        data  : "id=" + slug + "&userStatus=" + encodeURI(userStatus) ,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/collection/collector";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var saveData = function(){
    var userId = $("#userId").val();
    var userPass = $('#userPass').val();
    var userNama = $("#userNama").val();
    var userPonsel = $("#userPonsel").val();
    var userGroup = $("#userGroup").val();
    var userPrsh = $("#userPrsh").val();
    var userEmail = $("#userEmail").val();

    if(userId != "" && userPass != "" && userNama != "" && userPonsel != "" && userGroup != "" && userPrsh != "" && userEmail != "") {
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/user",
        data  : "userId=" + encodeURI(userId) + "&userPass=" + encodeURI(userPass) + "&userNama=" + encodeURI(userNama) + "&userPonsel=" + encodeURI(userPonsel) + "&userGroup=" + userGroup + "&userPrsh=" + userPrsh + "&userEmail=" + encodeURI(userEmail),
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/collection/collector";
            }, 500);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      }); 
    }
    else {
      toastr.error("Harap isi data dengan lengkap");
    }   
  }

  var deleteData = function(slug){
    //console.log(slug)
    swal({
      title: "Konfirmasi",
      text: "Hapus data user ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      createOverlay("Proses...");
      $.ajax({  
        type  : "DELETE",
        url   : "<?php echo asset_url(); ?>/user",
        data  : "id=" + slug,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/collection/collector";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var resetPassword = function(slug){
    swal({
      title: "Konfirmasi",
      text: "Reset password user menjadi <i><?php echo getSetting('DEFAULT_PASSWORD'); ?></i> ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      //console.log(slug)
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/user/reset-password",
        data  : "id=" + slug,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/collection/collector";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var editData = function(userId) {
    $("#btnUpdateUser").attr("data-user-id", "");
    createOverlay("Proses...");
    $.ajax({  
      type  : "GET",
      url   : "<?php echo asset_url(); ?>/user/detail",
      data  : "id=" + userId,
      success : function(result) { 
        gOverlay.hide();
        var data = JSON.parse(result);

        if(data["STATUS"] == "SUCCESS") {            
          $('#mdlUser_Edit').on('shown.bs.modal', function() {
            $("#userIdEdit").val(data["PAYLOAD"]["U_ID"]);
            $("#userNamaEdit").val(data["PAYLOAD"]["U_NAMA"]);
            $("#userPonselEdit").val(data["PAYLOAD"]["U_TELPON"]);
            $("#userEmailEdit").val(data["PAYLOAD"]["U_EMAIL"]);

            $("#btnUpdateUser").attr("data-user-id", data["PAYLOAD"]["U_ID"]);

            $("#userNamaEdit").focus();
          }) 

          $("#mdlUser_Edit").modal("show");
        }
        else {
          //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
          toastr.error(data["MESSAGE"]);
        }
      },
      error : function(error) {   
        gOverlay.hide();
        alert("Network/server error\r\n" + error);
      }
    });
  }

  var updateData = function() {
    var userId = $("#btnUpdateUser").attr("data-user-id");
    if(userId != "") {
      var userNama = $("#userNamaEdit").val();
      var userPonsel = $("#userPonselEdit").val();
      var userGroup = $("#userGroupEdit").val();
      var userPrsh = $("#userPrshEdit").val();
      var userEmail = $("#userEmailEdit").val();

      if(userPonsel != "" && userEmail != "") {
        createOverlay("Proses...");
        $.ajax({  
          type  : "PUT",
          url   : "<?php echo asset_url(); ?>/user",
          data  : "userId=" + encodeURI(userId) + "&userNama=" + encodeURI(userNama) + "&userPonsel=" + encodeURI(userPonsel) + "&userGroup=" + userGroup + "&userPrsh=" + userPrsh + "&userEmail=" + encodeURI(userEmail),
          success : function(result) { 
            gOverlay.hide();
            var data = JSON.parse(result);

            if(data["STATUS"] == "SUCCESS") {            
              toastr.success(data["MESSAGE"]);
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/collection/collector";
              }, 500);              
            }
            else {
              //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
              //toastr.error(data["MESSAGE"]);
              swal({
                title: "GAGAL",
                text: data["MESSAGE"],
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false,
                html: true
              },
              function(){
                setTimeout(function(){ 
                  window.location = "<?php echo asset_url(); ?>/collection/collector";
                }, 500);              
              });
            }
          },
          error : function(error) {   
            gOverlay.hide();
            alert("Gangguan pada server/jaringan\r\n" + error);
          }
        });
      }
      else {
        toastr.error("Nomor ponsel dan email HARUS diisi");
      }    
    }
  }
</script>
@stop