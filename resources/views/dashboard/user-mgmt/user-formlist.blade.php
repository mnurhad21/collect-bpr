@extends('dashboard.layout-dashboard')
@section('content')
<?php include_once('../application/helpers/helper.php'); ?>
<div class="page-header">
  <div class="page-header-content">
    <div class="page-title">
      <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Manajemen User</span></h4>
    </div>
    <!--
    <div class="heading-elements">
      <div class="heading-btn-group">
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
      </div>
    </div>
    //-->
  </div>

  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li><a href="<?php echo asset_url(); ?>"><i class="icon-home2 position-left"></i> Beranda</a></li>
      <li><i class="icon-menu position-left"></i> General</a></li>
      <li class="active"><i class="icon-users"></i> Manajemen User</li>
    </ul>
    <!--
    <ul class="breadcrumb-elements">
      <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="icon-gear position-left"></i>
          Settings
          <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
          <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
          <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
          <li class="divider"></li>
          <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
        </ul>
      </li>
    </ul>
    //-->
  </div>
</div>

<div class="content">
  <form class="form-horizontal" method="post" id="formData" name="formData"> 
    <div class="panel panel-flat border-top-primary">
      <div class="panel-heading">
        <h5 class="panel-title text-semibold" style="color:rgb(51, 150, 213);">Tambah User</h5>
        <div class="heading-elements">
          <ul class="icons-list">  
            <li><a data-action="collapse"></a></li>
          </ul>
        </div>
      </div>

      <div class="panel-body">
        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Perusahaan</label>
          <div class="col-lg-4">
            <select class="form-control" name="userPrsh" id="userPrsh">
              <option value="-">[Non-perusahaan]</option>
              <?php
              if(isset($ctlPrsh) && count($ctlPrsh) > 0) {
                foreach ($ctlPrsh as $aData) {
                  ?>
                  <option value="<?php echo $aData->{"PRSH_ID"}; ?>"><?php echo $aData->{"PRSH_NAMA"}; ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">User ID</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-user"></i></span>
              <input type="text" class="form-control first-input" id="userId" name="userId" placeholder="User ID...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Password</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
              <input type="password" class="form-control" id="userPass" name="userPass" placeholder="Password...">
            </div>                        
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Nama</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-profile"></i></span>
              <input type="text" class="form-control" id="userNama" name="userNama" placeholder="Nama user...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">No.Ponsel</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-phone2"></i></span>
              <input type="text" class="form-control" id="userPonsel" name="userPonsel" placeholder="Nomor ponsel user...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Email</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-mention"></i></span>
              <input type="text" class="form-control" id="userEmail" name="userEmail" placeholder="Email user...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Group Role</label>
          <div class="col-lg-4">
            <select class="form-control" name="userGroup" id="userGroup">
              <?php
              if(isset($ctlRefGroupRole) && count($ctlRefGroupRole) > 0) {
                foreach ($ctlRefGroupRole as $aData) {
                  ?>
                  <option value="<?php echo $aData->{"R_ID"}; ?>"><?php echo $aData->{"R_INFO"}; ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
        </div>
       
        <div class="form-group">
          <label class="col-lg-4 control-label text-semibold">&nbsp;</label>            
          <div class="col-lg-2">
            <button type="button" class="btn bg-green btn-block btn-labeled" onClick="saveData()"><b><i class="fa fa-check position-right"></i></b> Simpan</button>
          </div>            
        </div>
      </div>
    </div>

    <div class="panel panel-flat border-top-primary">
      <div class="panel-heading">
        <h5 class="panel-title text-semibold" style="color:rgb(51, 150, 213);">Data User</h5>
        <div class="heading-elements">
          <ul class="icons-list">  
            <li><a data-action="collapse"></a></li>
          </ul>
        </div>
      </div>
      <div class="panel-body">
        <table class="table datatable-basic">
          <thead>
            <tr>
              <th style="text-align:center" width="18%">Perusahaan</th>
              <th style="text-align:center" width="16%">User ID</th>
              <th style="text-align:center" width="20%">Nama</th>
              <th style="text-align:center" width="15%">Status</th>
              <th style="text-align:center" width="15%">Group Role</th>
              <th style="text-align:center" width="13%">Tindakan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(isset($ctlAppUsers) && count($ctlAppUsers) > 0) {
              foreach ($ctlAppUsers as $aData) {
                if($aData->{"U_STATUS"} == "USER_ACTIVE")    $btnClass = "btn-success";
                if($aData->{"U_STATUS"} == "USER_INACTIVE")  $btnClass = "btn-danger";
                ?>
                <tr>
                  <td><b><?php echo $aData->{"PRSH_NAMA"}; ?></b></td>
                  <td><b><?php echo $aData->{"U_ID"}; ?></b></td>
                  <td><?php echo $aData->{"U_NAMA"}; ?></td>
                  <td style="text-align:center">
                    <div id="divAktif_<?php echo $aData->{"U_ID"}; ?>_Label" style="display:none"><span class="clickable" data-id="<?php echo $aData->{"U_ID"}; ?>"><?php echo getReferenceInfo("USER_STATUS",$aData->{"U_STATUS"}); ?></span></div>
                    <div id="divAktif_<?php echo $aData->{"U_ID"}; ?>_Edit" style="display:block">
                      <div class="btn-group">
                        <button type="button" class="btn <?php echo $btnClass; ?> btn-icon dropdown-toggle" data-toggle="dropdown">
                          <?php echo strtoupper(getReferenceInfo("USER_STATUS",$aData->{"U_STATUS"})); ?> &nbsp;<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="javascript:setStatus('AKTIF','<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-check"></i> Aktif</a></li>
                          <li><a href="javascript:setStatus('NON_AKTIF','<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-remove"></i> Non-Aktif</a></li>
                          <!--li><a href="javascript:setStatus('SUSPEND','<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-ban"></i> Suspend</a></li-->
                        </ul>
                      </div>
                    </div>
                  </td>
                  <td style="text-align:center"><?php echo $aData->{"R_INFO"}; ?></td>
                  <td style="text-align:center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-icon dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="javascript:editData('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-edit"></i> Ubah</a></li>
                        <li><a href="javascript:resetPassword('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-retweet"></i> Reset Password</a></li>
                        <li><a href="javascript:deleteData('<?php echo $aData->{"U_ID"}; ?>')"><i class="fa fa-remove"></i> Hapus</a></li>
                        <?php
                        if($aData->{"U_GROUP_ROLE"} == "GR_COLLECTOR" && $aData->{"U_LOGIN_TOKEN"} != "" && $aData->{"U_LOGIN_TOKEN"} != "-") {
                          ?>
                          <!--li><a href="javascript:deleteToken('<?php echo $aData->{"U_ID"}; ?>')"><i class="icon-exit2"></i> Logout Aplikasi Mobile</a></li-->
                          <?php
                        }
                        ?>                        
                      </ul>
                    </div>
                  </td>
                </tr>      
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </form>   
  <div class="footer text-muted"></div>

  <div id="mdlUser_Edit" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h6 class="modal-title"><i class="icon-user"></i> &nbsp; Edit User</h6>
        </div>

        <div class="modal-body">
          <form class="form-horizontal" method="post" id="formCost" name="formCost" action="">    
            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Perusahaan</label>
              <div class="col-lg-8">
                <select class="form-control" name="userPrshEdit" id="userPrshEdit">
                  <option value="-">[Non-perusahaan]</option>
                  <?php
                  if(isset($ctlPrsh) && count($ctlPrsh) > 0) {
                    foreach ($ctlPrsh as $aData) {
                      ?>
                      <option value="<?php echo $aData->{"PRSH_ID"}; ?>"><?php echo $aData->{"PRSH_NAMA"}; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">User ID</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-user"></i></span>
                  <input type="text" id="userIdEdit" class="form-control" readonly>
                </div>
              </div>            
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Nama</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-profile"></i></span>
                  <input type="text" id="userNamaEdit" class="form-control">
                </div>
              </div>            
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">No.Ponsel</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-phone2"></i></span>
                  <input type="text" id="userPonselEdit" class="form-control">
                </div>
              </div>            
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Email</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-mention"></i></span>
                  <input type="text" class="form-control" id="userEmailEdit" name="userEmailEdit" placeholder="Email user...">
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Group Role</label>
              <div class="col-lg-8">
                <select class="form-control" id="userGroupEdit">
                  <?php
                  if(isset($ctlRefGroupRole) && count($ctlRefGroupRole) > 0) {
                    foreach ($ctlRefGroupRole as $aData) {
                      ?>
                      <option value="<?php echo $aData->{"R_ID"}; ?>"><?php echo $aData->{"R_INFO"}; ?></option>
                      <?php
                    }
                  }
                  ?>                
                </select>
              </div>
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-warning btn-labeled btn-xs" data-dismiss="modal"><b><i class="icon-cross3"></i></b> Tutup </button>
          <button type="button" class="btn btn-primary btn-labeled btn-xs" onClick="updateData();" id="btnUpdateUser" data-user-id=""><b><i class="icon-checkmark2"></i></b> Simpan </button>
        </div>
      </div>
    </div>
  </div>  
</div>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/pages/form_select2.js"></script>
<script type="text/javascript">
  // Select with search
  $('.select-search').select2();
</script>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/jquery.form.js"></script>
<script type="text/javascript">
  $(function() {
    // Table setup
    // ------------------------------
    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,        
        /*
        columnDefs: [{ 
            orderable: false,
            width: '80px',
            targets: [ 3 ]
        }],*/
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Cari &nbsp;</span> _INPUT_',
            lengthMenu: '<span>Tampil &nbsp;</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Datatable with saving state
    $('.datatable-basic').DataTable({
        stateSave: true,
        "order": [[ 0, "desc" ]]
    });

    // External table additions
    // ------------------------------
    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Kata kunci...');

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });    
  });

  $(".first-input").focus();

  var editData = function(userId) {
    $("#btnUpdateUser").attr("data-user-id", "");
    createOverlay("Proses...");
    $.ajax({  
      type  : "GET",
      url   : "/user/details",
      data  : "id=" + userId,
      success : function(result) { 
        gOverlay.hide();
        var data = JSON.parse(result);

        if(data["STATUS"] == "SUCCESS") {            
          $('#mdlUser_Edit').on('shown.bs.modal', function() {
            $("#userIdEdit").val(data["PAYLOAD"]["U_ID"]);
            $("#userNamaEdit").val(data["PAYLOAD"]["U_NAMA"]);
            $("#userPonselEdit").val(data["PAYLOAD"]["U_TELPON"]);
            $("#userGroupEdit").val(data["PAYLOAD"]["U_GROUP_ROLE"]);
            $("#userPrshEdit").val(data["PAYLOAD"]["PRSH_ID"]);
            $("#userEmailEdit").val(data["PAYLOAD"]["U_EMAIL"]);

            $("#btnUpdateUser").attr("data-user-id", data["PAYLOAD"]["U_ID"]);

            $("#userNamaEdit").focus();
          }) 

          $("#mdlUser_Edit").modal("show");
        }
        else {
          //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
          toastr.error(data["MESSAGE"]);
        }
      },
      error : function(error) {   
        gOverlay.hide();
        alert("Network/server error\r\n" + error);
      }
    });
  }

  var updateData = function() {
    var userId = $("#btnUpdateUser").attr("data-user-id");
    if(userId != "") {
      var userNama = $("#userNamaEdit").val();
      var userPonsel = $("#userPonselEdit").val();
      var userGroup = $("#userGroupEdit").val();
      var userPrsh = $("#userPrshEdit").val();
      var userEmail = $("#userEmailEdit").val();

      if(userPonsel != "" && userEmail != "") {
        createOverlay("Proses...");
        $.ajax({  
          type  : "PUT",
          url   : "<?php echo asset_url(); ?>/user",
          data  : "userId=" + encodeURI(userId) + "&userNama=" + encodeURI(userNama) + "&userPonsel=" + encodeURI(userPonsel) + "&userGroup=" + userGroup + "&userPrsh=" + userPrsh + "&userEmail=" + encodeURI(userEmail),
          success : function(result) { 
            gOverlay.hide();
            var data = JSON.parse(result);

            if(data["STATUS"] == "SUCCESS") {            
              toastr.success(data["MESSAGE"]);
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/user";
              }, 500);              
            }
            else {
              //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
              //toastr.error(data["MESSAGE"]);
              swal({
                title: "GAGAL",
                text: data["MESSAGE"],
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false,
                html: true
              },
              function(){
                setTimeout(function(){ 
                  window.location = "<?php echo asset_url(); ?>/user";
                }, 500);              
              });
            }
          },
          error : function(error) {   
            gOverlay.hide();
            alert("Gangguan pada server/jaringan\r\n" + error);
          }
        });
      }
      else {
        toastr.error("Nomor ponsel dan email HARUS diisi");
      }    
    }
  }

  var saveData = function(){
    var userId = $("#userId").val();
    var userPass = $('#userPass').val();
    var userNama = $("#userNama").val();
    var userPonsel = $("#userPonsel").val();
    var userGroup = $("#userGroup").val();
    var userPrsh = $("#userPrsh").val();
    var userEmail = $("#userEmail").val();

    if(userId != "" && userPass != "" && userNama != "" && userPonsel != "" && userGroup != "" && userPrsh != "" && userEmail != "") {
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/user",
        data  : "userId=" + encodeURI(userId) + "&userPass=" + encodeURI(userPass) + "&userNama=" + encodeURI(userNama) + "&userPonsel=" + encodeURI(userPonsel) + "&userGroup=" + encodeURI(userGroup) + "&userPrsh=" + encodeURI(userPrsh) + "&userEmail=" + encodeURI(userEmail),
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/user";
            }, 500);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/user";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      }); 
    }
    else {
      toastr.error("Harap isi data dengan lengkap");
    }   
  }

  var deleteData = function(slug){
    //console.log(slug)
    swal({
      title: "Konfirmasi",
      text: "Hapus data user ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      createOverlay("Proses...");
      $.ajax({  
        type  : "DELETE",
        url   : "<?php echo asset_url(); ?>/user",
        data  : "id=" + slug,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/user";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/user";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var resetPassword = function(slug){
    swal({
      title: "Konfirmasi",
      text: "Reset password user menjadi <i><?php echo getSetting('DEFAULT_PASSWORD'); ?></i> ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      //console.log(slug)
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/user/reset-password",
        data  : "id=" + slug,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/user";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/user";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var setStatus = function(userStatus,slug){
    swal({
      title: "Konfirmasi",
      text: "Ubah status user ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/user/update-status",
        data  : "id=" + slug + "&userStatus=" + encodeURI(userStatus) ,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/user";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/user";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var deleteToken = function(userId) {

  }
</script>
@stop