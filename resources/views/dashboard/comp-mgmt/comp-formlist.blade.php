@extends('dashboard.layout-dashboard')
@section('content')
<?php include_once('../application/helpers/helper.php'); ?>
<div class="page-header">
  <div class="page-header-content">
    <div class="page-title">
      <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Manajemen Perusahaan</span></h4>
    </div>
    <!--
    <div class="heading-elements">
      <div class="heading-btn-group">
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
      </div>
    </div>
    //-->
  </div>

  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li><a href="<?php echo asset_url(); ?>"><i class="icon-home2 position-left"></i> Beranda</a></li>
      <li><i class="icon-menu position-left"></i> General</a></li>
      <li class="active"><i class="icon-office"></i> Manajemen Perusahaan</li>
    </ul>
    <!--
    <ul class="breadcrumb-elements">
      <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="icon-gear position-left"></i>
          Settings
          <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
          <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
          <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
          <li class="divider"></li>
          <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
        </ul>
      </li>
    </ul>
    //-->
  </div>
</div>

<div class="content">
  <form class="form-horizontal" id="formData" name="formData" enctype="multipart/form-data" method="post" action="<?php echo asset_url(); ?>/company"> 
    <div class="panel panel-flat border-top-primary">
      <div class="panel-heading">
        <h5 class="panel-title text-semibold" style="color: #af1512;">Tambah Perusahaan</h5>
        <div class="heading-elements">
          <ul class="icons-list">  
            <li><a data-action="collapse"></a></li>
          </ul>
        </div>
      </div>

      <div class="panel-body">
        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">ID Perusahaan</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><strong><?php echo getSetting("COMPANY_PREFIX"); ?></strong></span>
              <input type="text" class="form-control first-input text-bold" id="prshId" name="prshId" placeholder="ID perusahaan...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Nama Perusahaan</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-institution"></i></span>
              <input type="text" class="form-control" id="prshNama" name="prshNama" placeholder="Nama perusahaan...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Alamat</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-location4"></i></span>
              <input type="text" class="form-control" id="prshAlamat" name="prshAlamat" placeholder="Alamat perusahaan...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Kota</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class="icon-city"></i></span>
              <input type="text" class="form-control" id="prshKota" name="prshKota" placeholder="Kota...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Penanggung Jawab</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class=" icon-user-tie"></i></span>
              <input type="text" class="form-control" id="prshPICNama" name="prshPICNama" placeholder="Nama penanggung jawab...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Telepon</label>
          <div class="col-lg-4">
            <div class="input-group">
              <span class="input-group-addon"><i class=" icon-phone"></i></span>
              <input type="text" class="form-control" id="prshPICTelp" name="prshPICTelp" placeholder="No.telepon penanggung jawab...">
            </div>            
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-2 control-label text-semibold">Logo Perusahaan</label>
          <div class="col-lg-4">
            <input type="file" class="file-input-img" data-show-preview="false" data-show-upload="false" id="imgFile" name="imgFile" placeholder="File format JPG atau PNG...">
          </div>            
        </div>
   
        <div class="form-group">
          <label class="col-lg-4 control-label text-semibold">&nbsp;</label>            
          <div class="col-lg-2">
            <button class="btn bg-green btn-block btn-labeled" onClick="saveData()"><b><i class="fa fa-check position-right"></i></b> Simpan</button>
          </div>            
        </div>
      </div>
    </div>

    <div class="panel panel-flat border-top-primary">
      <div class="panel-heading">
        <h5 class="panel-title text-semibold" style="color: #af1512;">Data Perusahaan</h5>
        <div class="heading-elements">
          <ul class="icons-list">  
            <li><a data-action="collapse"></a></li>
          </ul>
        </div>
      </div>
      <div class="panel-body">
        <table class="table datatable-basic">
          <thead>
            <tr>
              <th style="text-align:center" width="15%">ID</th>
              <th style="text-align:center" width="20%">Nama Perusahaan</th>
              <th style="text-align:center" width="13%">Aktif</th>
              <th style="text-align:center" width="15%">Supervisor</th>
              <th style="text-align:center" width="15%">Collector</th>
              <th style="text-align:center" width="15%">Data</th>
              <th style="text-align:center" width="15%">Tindakan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(isset($ctlPrsh) && count($ctlPrsh) > 0) {
              foreach ($ctlPrsh as $aData) {
                if($aData->{"PRSH_STATUS_AKTIF"} == "Y")  $btnClass = "btn-success";
                if($aData->{"PRSH_STATUS_AKTIF"} == "T")  $btnClass = "btn-danger";
                ?>
                <tr>
                  <td><b><?php echo $aData->{"PRSH_ID"}; ?></b></td>
                  <td><b><?php echo $aData->{"PRSH_NAMA"}; ?></b></td>
                  <td style="text-align:center">
                    <div id="divAktif_<?php echo $aData->{"PRSH_ID"}; ?>_Label" style="display:none"><span class="clickable" data-id="<?php echo $aData->{"PRSH_ID"}; ?>"><?php echo getReferenceInfo("DEFAULT_YES_NO",$aData->{"PRSH_STATUS_AKTIF"}); ?></span></div>
                    <div id="divAktif_<?php echo $aData->{"PRSH_ID"}; ?>_Edit" style="display:block">
                      <div class="btn-group">
                        <button type="button" class="btn <?php echo $btnClass; ?> btn-icon dropdown-toggle" data-toggle="dropdown">
                          <?php echo strtoupper(getReferenceInfo("DEFAULT_YES_NO",$aData->{"PRSH_STATUS_AKTIF"})); ?> &nbsp;<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="javascript:setStatus('AKTIF','<?php echo $aData->{"PRSH_ID"}; ?>')"><i class="fa fa-check"></i> Aktif</a></li>
                          <li><a href="javascript:setStatus('NON_AKTIF','<?php echo $aData->{"PRSH_ID"}; ?>')"><i class="fa fa-remove"></i> Non-Aktif</a></li>
                        </ul>
                      </div>
                    </div>
                  </td>
                  <td style="text-align:center"><?php echo $aData->{"JUMLAH_SPV"}; ?></td>
                  <td style="text-align:center"><?php echo $aData->{"JUMLAH_COLL"}; ?></td>
                  <td style="text-align:center"><?php echo $aData->{"JUMLAH_DATA"}; ?></td>
                  <td style="text-align:center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-icon dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="javascript:editData('<?php echo $aData->{"PRSH_ID"}; ?>')"><i class="fa fa-edit"></i> Ubah</a></li>
                        <li><a href="javascript:deleteData('<?php echo $aData->{"PRSH_ID"}; ?>')"><i class="fa fa-remove"></i> Hapus</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>      
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </form>   
  <div class="footer text-muted"></div>

  <div id="mdlCompany_Edit" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h6 class="modal-title"><i class="icon-user"></i> &nbsp; Edit Perusahaan</h6>
        </div>

        <div class="modal-body">
          <form class="form-horizontal" method="post" id="formPerusahaan" name="formPerusahaan" action="">    
            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">ID Perusahaan</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-office"></i></span>
                  <input type="text" class="form-control first-input text-bold" id="prshIdEdit" placeholder="ID perusahaan..." readonly>
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Nama Perusahaan</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-institution"></i></span>
                  <input type="text" class="form-control" id="prshNamaEdit" placeholder="Nama perusahaan...">
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Alamat</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-location4"></i></span>
                  <input type="text" class="form-control" id="prshAlamatEdit" placeholder="Alamat perusahaan...">
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Kota</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-city"></i></span>
                  <input type="text" class="form-control" id="prshKotaEdit" placeholder="Kota...">
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Penanggung Jawab</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class=" icon-user-tie"></i></span>
                  <input type="text" class="form-control" id="prshPICNamaEdit" placeholder="Nama penanggung jawab...">
                </div>            
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-4 control-label text-semibold">Telepon</label>
              <div class="col-lg-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class=" icon-phone"></i></span>
                  <input type="text" class="form-control" id="prshPICTelpEdit" placeholder="No.telepon penanggung jawab...">
                </div>            
              </div>
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-warning btn-labeled btn-xs" data-dismiss="modal"><b><i class="icon-cross3"></i></b> Tutup </button>
          <button type="button" class="btn btn-primary btn-labeled btn-xs" onClick="updateData();" id="btnUpdateCompany" data-prsh-id=""><b><i class="icon-checkmark2"></i></b> Simpan </button>
        </div>
      </div>
    </div>
  </div>  
</div>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/pages/form_select2.js"></script>
<script type="text/javascript">
  // Select with search
  $('.select-search').select2();
</script>

<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>/assets/js/jquery.form.js"></script>
<script type="text/javascript">
  $(function() {
    // Table setup
    // ------------------------------
    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,        
        /*
        columnDefs: [{ 
            orderable: false,
            width: '80px',
            targets: [ 3 ]
        }],*/
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Cari &nbsp;</span> _INPUT_',
            lengthMenu: '<span>Tampil &nbsp;</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Datatable with saving state
    $('.datatable-basic').DataTable({
        stateSave: true,
        "order": [[ 0, "desc" ]]
    });

    // External table additions
    // ------------------------------
    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Kata kunci...');

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    }); 

    $('.file-input-img').fileinput({
      browseLabel: '',
      browseClass: 'btn btn-primary btn-icon',
      removeLabel: '',
      uploadLabel: '',
      uploadClass: 'btn btn-default btn-icon',
      browseIcon: '<i class="icon-plus22"></i> ',
      uploadIcon: '<i class="icon-file-upload"></i> ',
      removeClass: 'btn btn-danger btn-icon',
      removeIcon: '<i class="icon-cancel-square"></i> ',
      layoutTemplates: {
          caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
      },
      initialCaption: "Pilih file JPG atau PNG",
      /*initialCaption: "Pilih file",*/
      allowedFileExtensions: ["jpg", "jpeg", "png"]
    });   
  });

  var gBusy = false;

  $(".first-input").focus();
  $("#prshId").val("<?php echo $ctlUrutan ?>");

  var editData = function(prshId) {
    $("#btnUpdateCompany").attr("data-prsh-id", "");
    createOverlay("Proses...");
    $.ajax({  
      type  : "GET",
      url   : "<?php echo asset_url(); ?>/company/detail",
      data  : "id=" + prshId,
      success : function(result) { 
        gOverlay.hide();
        var data = JSON.parse(result);

        if(data["STATUS"] == "SUCCESS") {            
          $('#mdlCompany_Edit').on('shown.bs.modal', function() {
            $("#prshIdEdit").val(data["PAYLOAD"]["PRSH_ID"]);
            $("#prshNamaEdit").val(data["PAYLOAD"]["PRSH_NAMA"]);
            $("#prshAlamatEdit").val(data["PAYLOAD"]["PRSH_ALAMAT"]);
            $("#prshKotaEdit").val(data["PAYLOAD"]["PRSH_KOTA"]);
            $("#prshPICNamaEdit").val(data["PAYLOAD"]["PRSH_PIC_NAMA"]);
            $("#prshPICTelpEdit").val(data["PAYLOAD"]["PRSH_PIC_TELP"]);

            $("#btnUpdateCompany").attr("data-prsh-id", data["PAYLOAD"]["PRSH_ID"]);

            $("#prshNamaEdit").focus();
          }) 

          $("#mdlCompany_Edit").modal("show");
        }
        else {
          //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
          toastr.error(data["MESSAGE"]);
        }
      },
      error : function(error) {   
        gOverlay.hide();
        alert("Network/server error\r\n" + error);
      }
    });
  }

  var updateData = function() {
    var prshId = $("#btnUpdateCompany").attr("data-prsh-id");
    if(prshId != "") {
      var prshNama = $("#prshNamaEdit").val();
      var prshAlamat = $("#prshAlamatEdit").val();
      var prshKota = $("#prshKotaEdit").val();
      var prshPICNama = $("#prshPICNamaEdit").val();
      var prshPICTelp = $("#prshPICTelpEdit").val();

      if(prshNama != "" && prshAlamat != "" && prshKota != "" && prshPICNama != "" && prshPICTelp != "") {
        createOverlay("Proses...");
        $.ajax({  
          type  : "PUT",
          url   : "<?php echo asset_url(); ?>/company",
          data  : "prshId=" + encodeURI(prshId) + "&prshNama=" + encodeURI(prshNama) + "&prshAlamat=" + encodeURI(prshAlamat) + "&prshKota=" + encodeURI(prshKota) + "&prshPICNama=" + encodeURI(prshPICNama) + "&prshPICTelp=" + encodeURI(prshPICTelp),
          success : function(result) { 
            gOverlay.hide();
            var data = JSON.parse(result);

            if(data["STATUS"] == "SUCCESS") {            
              toastr.success(data["MESSAGE"]);
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/company";
              }, 500);              
            }
            else {
              //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
              //toastr.error(data["MESSAGE"]);
              swal({
                title: "GAGAL",
                text: data["MESSAGE"],
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false,
                html: true
              },
              function(){
                setTimeout(function(){ 
                  window.location = "<?php echo asset_url(); ?>/company";
                }, 500);              
              });
            }
          },
          error : function(error) {   
            gOverlay.hide();
            alert("Gangguan pada server/jaringan\r\n" + error);
          }
        });
      }
      else {
        toastr.error("Isikan data dengan lengkap");
      }    
    }
  }
  /*
  var saveData = function(){
    var prshId = $("#prshId").val();
    var prshNama = $("#prshNama").val();
    var prshAlamat = $("#prshAlamat").val();
    var prshKota = $("#prshKota").val();
    var prshPICNama = $("#prshPICNama").val();
    var prshPICTelp = $("#prshPICTelp").val();

    if(prshId != "" && prshNama != "" && prshAlamat != "" && prshKota != "" && prshPICNama != "" && prshPICTelp != "") {
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/company",
        data  : "prshId=" + encodeURI(prshId) + "&prshNama=" + encodeURI(prshNama) + "&prshAlamat=" + encodeURI(prshAlamat) + "&prshKota=" + encodeURI(prshKota) + "&prshPICNama=" + encodeURI(prshPICNama) + "&prshPICTelp=" + encodeURI(prshPICTelp),
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/company";
            }, 500);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/company";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      }); 
    }
    else {
      toastr.error("Harap isi data dengan lengkap");
    }   
  }
  */
  var deleteData = function(slug){
    //console.log(slug)
    swal({
      title: "Konfirmasi",
      text: "Hapus data perusahaan ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      createOverlay("Proses...");
      $.ajax({  
        type  : "DELETE",
        url   : "<?php echo asset_url(); ?>/company",
        data  : "id=" + slug,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/company";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/company";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var setStatus = function(prshStatus,slug){
    swal({
      title: "Konfirmasi",
      text: "Ubah status perusahaan ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya",
      cancelButtonText : "Tidak",
      closeOnConfirm: true,
      html: true
    },
    function(){        
      createOverlay("Proses...");
      $.ajax({  
        type  : "POST",
        url   : "<?php echo asset_url(); ?>/company/update-status",
        data  : "id=" + slug + "&prshStatus=" + encodeURI(prshStatus) ,
        success : function(result) { 
          gOverlay.hide();
          var data = JSON.parse(result);

          if(data["STATUS"] == "SUCCESS") {            
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/company";
            }, 300);              
          }
          else {
            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
            //toastr.error(data["MESSAGE"]);
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/company";
              }, 500);              
            });
          }
        },
        error : function(error) {   
          gOverlay.hide();
          alert("Gangguan pada server/jaringan\r\n" + error);
        }
      });    
    });
  }

  var saveData = function() {    
    if(!gBusy) {
      gBusy = true;
      //- content action form
      var optFormContentUpdate = {
        beforeSubmit  : function(e) {
          $(".btn").attr("disabled","");
          createOverlay("Proses...");
          return true;
        }, 
        resetForm : false,
        success : function(result) {
          gBusy = false;
          gOverlay.hide();
          var data = JSON.parse(result);
          $(".btn").removeAttr("disabled");
          if(data["STATUS"] == "SUCCESS") {
            toastr.success(data["MESSAGE"]);
            setTimeout(function(){ 
              window.location = "<?php echo asset_url(); ?>/company";
            }, 500);            
            /*
            swal({
              title: "SUKSES",
              text: data["MESSAGE"],
              type: "success",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: false,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/company";
              }, 500);
            });
            */
          }
          else {
            //toastr.error(data["MESSAGE"]);            
            swal({
              title: "GAGAL",
              text: data["MESSAGE"],
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "OK",
              closeOnConfirm: true,
              html: true
            },
            function(){
              setTimeout(function(){ 
                window.location = "<?php echo asset_url(); ?>/company";
              }, 500);              
            });
          }        
          //$(".btn").removeAttr("disabled");
          //window.location = "<?php echo asset_url(); ?>/company";
        },
        error : function(e) {
          gBusy = false;
          gOverlay.hide();

          toastr.error("Proses <b>GAGAL</b>");     
          $(".btn").removeAttr("disabled");   
          window.location = "<?php echo asset_url(); ?>/company";
        }
      };  
      $('#formData').submit(function() {
        $(this).ajaxSubmit(optFormContentUpdate); 
        // return false to prevent standard browser submit and page navigation
        return false;
      });
    }
  }
</script>
@stop