<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/dd', function () {
    return view('welcome');
});

date_default_timezone_set('Asia/Jakarta');


Route::get("/","LoginCollectController@showLoginCollect");
Route::post("login","LoginCollectController@processLogin")->name("login");
Route::get("/logout","LoginCollectController@processLogout");
Route::post("/logout","LoginCollectController@processLogout");

Route::get("/dashboard","DashboardController@dashboardMain");

Route::get("/collection/collector", "CollectionController@formlistCollector");
Route::post("/collection/collector", "CollectionController@submitCollector");
Route::get("/collection/collector/detail", "CollectionController@listDetailCollector");

Route::get("/collection/jadwal-penagihan", "CollectionController@formlistJadwal");
Route::post("/collection/jadwal-penagihan", "CollectionController@submitJadwal");
Route::delete("/collection/jadwal-penagihan", "CollectionController@deleteJadwal");
Route::get("/collection/jadwal-penagihan/{buId}", "CollectionController@listDetailJadwal");

Route::get("/collection/monitoring", "CollectionController@displayMonitoring");
Route::post("/collection/monitoring", "CollectionController@displayMonitoring");
Route::get("/collection/monitoring/position", "CollectionController@getPosition");

Route::get("/collection/laporan", "CollectionController@listLaporan");
Route::get("/collection/laporan/download", "CollectionController@downloadLaporan");

Route::get("/collection/api/jadwal", "CollectionController@apiListJadwal");
Route::post("/collection/api/jadwal", "CollectionController@apiListJadwal");
Route::post("/collection/api/jadwal/update", "CollectionController@apiUpdateJadwal");

Route::get("/collection/api/receipt", "CollectionController@apiGetReceipt");
Route::post("/collection/api/receipt", "CollectionController@apiGetReceipt");

Route::get("/collection/api/dashboard", "CollectionController@apiGetSummary");
Route::post("/collection/api/dashboard", "CollectionController@apiGetSummary");

Route::get("/collection/api/start-check-in", "CollectionController@apiRegisterStartCheckIn");
Route::post("/collection/api/start-check-in", "CollectionController@apiRegisterStartCheckIn");

Route::get("/collection", function() {
  return Redirect::to("/collection/jadwal-penagihan");
});
Route::post("/collection", function() {
  return Redirect::to("/collection/jadwal-penagihan");
});

//user management
Route::get("/user", "UserController@listUsers");
Route::get("/user/detail", "UserController@getUserData");
Route::post("/user", "UserController@addUser");
Route::put("/user", "UserController@updateUser");
Route::delete("/user", "UserController@deleteUser");

Route::post("/user/reset-password", "UserController@resetUserPassword");
Route::get("/user/reset-password", "UserController@resetUserPassword");

Route::post("/user/update-status", "UserController@updateUserStatus");
Route::get("/user/update-status", "UserController@updateUserStatus");

//company management
Route::get("/company", "CompanyController@listCompanies");
Route::get("/company/detail", "CompanyController@getCompanyData");
Route::post("/company", "CompanyController@addCompany");
Route::put("/company", "CompanyController@updateCompany");
Route::delete("/company", "CompanyController@deleteCompany");

Route::post("/company/update-status", "CompanyController@updateCompanyStatus");
Route::get("/company/update-status", "CompanyController@updateCompanyStatus");

//profile
Route::get("/profil", "ProfileController@formProfile");
Route::put("/profil", "ProfileController@updateProfile");

Route::get("/settings", "SettingController@formSettings");
Route::post("/settings", "SettingController@updateSettings");

Route::get("/ref/{refCategory}", "CollectionController@getReference");

//admin settings
Route::get("/adm/settings", "SettingController@formAdmSettings");
Route::post("/adm/settings", "SettingController@updateAdmSettings");

//testing
Route::get("/tes", "LoginController@checkToken");
Route::get("/tes1", "DashboardController@tesTgl");
Route::get("/tes3", function() {
  $dateA = ("2017-08-09 10:22:34");
  $dateB = ("2017-08-10 15:11:11");
  $arrDiff = dayDifference2($dateA, $dateB, false);
  echo "DAY : ".$arrDiff["DAY"]."<br>";
  echo "MONTH : ".$arrDiff["MONTH"]."<br>";
  echo "YEAR : ".$arrDiff["YEAR"]."<br>";
  echo "HOUR : ".$arrDiff["HOUR"]."<br>";
  echo "MINUTE : ".$arrDiff["MINUTE"]."<br>";
  echo "SECOND : ".$arrDiff["SECOND"]."<br>";
});

//Route::get("/server-time", function() {
//  echo tglIndo(date("Y-m-d H:i:s"), "SHORT");
//});
Route::get("/server-time", "DashboardController@tesTgl2");
